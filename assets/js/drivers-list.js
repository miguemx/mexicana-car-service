'use strict';

const e = React.createElement;

class HeaderTable extends React.Component {

    render() {
        return(
            <thead>
                <tr>
                    <th>Car</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Online</th>
                    <th>PIN</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
        );
    }
}

class BodyTable extends React.Component {

    state = {
        drivers: [
            
        ],
        totalDrivers: 0
    }

    componentDidMount() {
        this.getDriversList();
    }

    getDriversList = async () => {
        let response = await fetch(BASEFRONT + 'Drivers/lista').catch();
        let data = await response.json();
        console.log(data);
        this.setState({
            totalDrivers: data.data.rows,
            drivers: data.data.results
        });
    }

    render() {
        console.log(this.state);
        return(
            <tbody>
                {this.state.drivers.map(row => (
                    <tr key={row.driverId}>
                        <td>{row.car}</td>
                        <td>{row.givenname} {row.surname}</td>
                        <td>{row.status}</td>
                        <td>{(row.online=="1")?'Online':'Offline'}</td>
                        <td>{row.pin}</td>
                        <td>
                            <a href={BASEFRONT+'Drivers/edit/'+row.driverId} className="btn btn-sm btn-secondary">View / Edit</a>
                        </td>
                    </tr>
                ))}
            </tbody>
        );
    }
}

class TablaDrivers extends React.Component {

    render() {
        return(
            <table className="table table-bordered table-hover table-sm">
                <HeaderTable />
                <BodyTable />
            </table>
        );
    }

}


class ListaDrivers extends React.Component {

    render() {
        return(
            <div>
                <TablaDrivers />
            </div>
        );
    }

}



const domContainer = document.querySelector('#root');
ReactDOM.render( <ListaDrivers />, domContainer);
