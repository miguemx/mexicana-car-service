$(document).ready(function() {
    $("#btnSave").click(save);
});

const getForm = () => {
    var elements = document.getElementById("formEdit").elements;
    var obj ={};
    for(var i = 0 ; i < elements.length ; i++){
        var item = elements.item(i);
        obj[item.id] = item.value;
    }
    return obj;
}

const save = () => {
    var data = getForm();
    var url = BASEFRONT + 'Drivers/add';
    if ( data.driverId != '0' ) {
        var url = BASEFRONT + 'Drivers/update';
    }
    
    $.ajax({
        url: url,
        method: 'post',
        dataType: 'json',
        data: JSON.stringify(data),
        beforeSend: handleBeforeSend,
        error: handleError,
        success: function(response) {
            console.log(response);
        }
    });
}

const handleBeforeSend = () => {

}

const handleError = () => {

}

