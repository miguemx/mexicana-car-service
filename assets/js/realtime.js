// var socket = io('http://148.228.11.17:8080');
var socket = io('http://localhost:8080');

socket.on("newcall", function(data) {
    console.log("New trip added");
    console.log(data);
});

socket.on("location", function(data) {
    var myLatLng = {lat: data.latitude, lng: data.longitude};

    if ( typeof(markers[data.driverId]) == 'undefined' ) {
        markers[data.driverId] = new google.maps.Marker({});
    }

    if( typeof(markers[data.driverId]) == 'object' ) {
        markers[data.driverId].setPosition( myLatLng );
        markers[data.driverId].setTitle(data.driverId);
        markers[data.driverId].setLabel(data.driverId);
        markers[data.driverId].setIcon( BASEURL + 'assets/img/taxi.png' );
        markers[data.driverId].setMap( map );
    }
    /*
    markers[ data.driverId ] = new google.maps.Marker({
        position: myLatLng,
        label: data.driverId,
        icon: BASEURL + 'assets/img/taxi.png',
        map: map
    });
    
    if( typeof(marker) === 'object' ) {
        marker.setPosition( myLatLng );
        marker.setTitle('1005');
        marker.setLabel('1005');
        marker.setIcon( BASEURL + 'assets/img/taxi.png' );
        marker.setMap( map );
        
    }
    */
});

