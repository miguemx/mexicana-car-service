'use strict';

const e = React.createElement;

class TextInput extends React.Component {
    state = {
        value: ''
    }

    constructor(props) {
        super(props);
        
    }

    changeText = () => {
        this.setState({value: event.target.value});
        let obj = new Object();
        obj.key = this.props.llave;
        obj.value = this.state.value;
        this.props.hc(obj);
    }

    render() {
        console.log(this.props);
        return(
            <div className={this.props.classcol+' form-group'}>
                <label>{this.props.label}</label>
                <input type="text" 
                    className="form-control form-control-sm" 
                    placeholder={this.props.placeholder} 
                    onChange={this.changeText}
                    value={this.props.val} />
            </div>
        );
    }
}

class Select extends React.Component {

    render() {
        return(
            <div className={this.props.col+' form-group'}>
                <label for="">{this.props.label}</label>
                <input type="text" 
                    className="form-control form-control-sm" 
                    placeholder={this.props.placeholder} 
                    value={this.props.value} />
                <select className="form-control form-control-sm">
                    {this.props.options.map( option => (
                        <option key={option.value} value={option.value}>{option.text}</option>
                    ) )}
                </select>
            </div>
        );
    }
}

class EditDrivers extends React.Component {

    state = {
        values: [
            {key: "driverId" , value: "0"},     // 0
            {key: "car" , value:  "0"},         // 1
            {key: "imei" , value:  ""},         // 2
            {key: "pin" , value:  ""},          // 3
            {key: "status" , value:  "ACTIVE"}, // 4
            {key: "online" , value:  "0"},      // 5
            {key: "owner" , value:  "0"},       // 6
            {key: "drvdn" , value:  ""},        // 7
            {key: "licence" , value:  ""},      // 8
            {key: "licenceType" , value:  ""},  // 9
            {key: "licenceExp" , value:  ""},   // 10
            {key: "tlcLic" , value:  ""},       // 11
            {key: "tlcExpDate" , value:  ""},   // 12
            {key: "beginData" , value:  ""},    // 13
            {key: "tlcUrineExp" , value:  ""},  // 14

            {key: "personId" , value:  "0"},    // 15
            {key: "surname" , value:  ""},      // 16
            {key: "givenname" , value:  ""},    // 17
            {key: "address" , value:  ""},      // 18
            {key: "phone" , value:  ""},        // 19
            {key: "cellphone" , value:  ""},    // 20
            {key: "email" , value:  ""},        // 21
            {key: "city" , value:  ""},         // 22
            {key: "state" , value:  ""},        // 23
            {key: "zip" , value:  ""},          // 24
            {key: "birth" , value:  ""},        // 25
            {key: "ssNumber" , value:  ""},     // 26
            {key: "usCitizen" , value:  "0"}    // 27
        ]
    }

    constructor(props) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
    }

    getVal = (key) => {
        let value = "";
        for ( let i=0; i<this.state.values.length; i++ ) {
            if ( this.state.values[i].key == key ) {
                value = this.state.values[i].value;
            }
        }
        return value;
    }

    handleChange = (obj) => {
        let valuesState = this.state.values;
        for ( let i=0; i<this.state.values.length; i++ ) {
            if ( valuesState[i].key == obj.key ) {
                valuesState[i].value = obj.value;
            }
        }
        this.setState({values: valuesState});
    }

    render() {
        return(
            <div className="row">
                <div className="col-sm-12">
                    <TextInput classcol="col-sm-3" label="Driver ID" placeholder="0000" 
                        hc={this.handleChange} val={this.getVal("driverId")} llave="driverId" />
                </div>
            </div>
        );
    }
}

const domContainer = document.querySelector('#root');
ReactDOM.render( <EditDrivers />, domContainer);
