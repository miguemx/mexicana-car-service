$(document).ready(function() {
    
    $("#pickupAddress").focusout(calculateRoute);
    $("#destinationAddress").focusout(calculateRoute);
    
});

var map; // google maps map used in the dispatcher window
// var marker;
var markers = new Array();

/**
 * initializes google maps map
 */
function initMap() {
    // inicializar servicio de rutas
    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer;
    
    // inicializar mapa
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 40.930903, lng: -73.89545780000003},
        zoom: 14,
        streetViewControl: false,
        mapTypeControl: false,
        fullscreenControl: false
    });
    // inicializar el mapa para mostrar las rutas
    directionsDisplay.setMap(map);

    // definir los campos de autocompletado
    $(".maps-autocomplete").each(function() {
        let txtObj = $(this).get(0);
        createAutoComplete(txtObj);
    });

    // marker = new google.maps.Marker({});

}

/**
 * calculates route between 2 waypoints
 */
function calculateRoute() {
    var start = $("#pickupAddress").val();
    var end = $("#destinationAddress").val();
    
    if ( start.length > 0 && end.length > 0)  {
        var waypoints = [];
        
        $(".txt-waypoint").each(function() {
            waypoints.push({
                location: $(this).val(),
                stopover: true
            });
        });

        directionsService.route({
                origin: start,
                destination: end,
                travelMode: 'DRIVING',
                waypoints: waypoints,
                optimizeWaypoints: true
            }, 
            function(response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                    estimate(directionsDisplay.directions.routes[0].legs);
                } 
                else {
                    console.log('Directions request failed due to ' + status);
                }
            }
        );

        var request = {
            origin: start,
            destination: end,
            travelMode: 'DRIVING'
        };
        directionsService.route(request,function(result,status) {
            if ( status === 'OK' ) {
                directionsDisplay.setDirections(result);
                setCallValues(result);
            }
        });
    }
}

/**
 * adds autocomplete function to an input text in order to get google maps directions and places
 * @param {DOM OBJECT} domTxtObject dom object created by clause "document.getElementById"
 */
function createAutoComplete(domTxtObject) {
    let autocomplete = new google.maps.places.Autocomplete(domTxtObject);
    autocomplete.bindTo('bounds', map);

    var marker = new google.maps.Marker({map: map});

    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            return;
        }

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } 
        else {
            map.setCenter(place.geometry.location);
            map.setZoom(18);
        }

        // Set the position of the marker using the place ID and location.
        marker.setPlace({
            placeId: place.place_id,
            location: place.geometry.location
        });
        marker.setVisible( false );        
        getInfoPlace( domTxtObject.id, place.place_id, place.name, place.address_components, place.geometry.location.lat(), place.geometry.location.lng() );
    });
}

/**
 * adds an input field to write a google maps place
 */
function addWayPoint() {
    var breakline = document.createElement("br");
    document.getElementById("newcall-waypoints-inputs").appendChild(breakline); 

    var input = document.createElement("input");
    input.type = 'text';
    input.className = 'txt-waypoint maps-autocomplete';
    input.onblur = calculateRoute;
    document.getElementById("newcall-waypoints-inputs").appendChild(input);
    createAutoComplete(input)  
}

function getInfoPlace(type, placeId, name, address, lat, lng) {
    var place = new Object();
    place.id = placeId;
    place.name = name;
    place.streetNumber = searchFieldAddress(address,'street_number');
    place.street       = searchFieldAddress(address,'route');
    place.city         = (searchFieldAddress(address,'locality'))?searchFieldAddress(address,'locality'):searchFieldAddress(address,'administrative_area_level_2');
    place.state        = searchFieldAddress(address,'administrative_area_level_1');
    place.country      = searchFieldAddress(address,'country');
    place.postalCode   = searchFieldAddress(address,'postal_code');
    place.neighborhood = (searchFieldAddress(address,'neighborhood'))?searchFieldAddress(address,'neighborhood'):searchFieldAddress(address,'sublocality_level_1');
    place.latitude     = lat;
    place.longitude    = lng;

    var strPlace = JSON.stringify(place);
    
    if ( type == 'pickupAddress' ) {
        $("#pickup").val(strPlace);
    }
    else if ( type == 'destinationAddress' ) {
        $("#destination").val(strPlace);
    }
}

function searchFieldAddress(address,field) {
    var name = '';
    for ( var i=0; i<address.length; i++ ) {
        for ( var j=0; j<address[i].types.length; j++ ) {
            if ( address[i].types[j] == field ) {
                name = address[i].long_name;
                break;
            }
        }
    }
    return name;
}

function estimate(legs) {
    var startCost = 5;
    var costPerMile = 1;

    var distance = legs[0].distance.value / 1609.34;
    var cost = Math.round( (startCost + distance * costPerMile) * 100 ) / 100;

    $("#distance").val(legs[0].distance.text);
    $("#eta").val(legs[0].duration.text);
    $("#amount").val( cost );


}

function writeLocation(data) {
    console.log(data);
    let latitud = data.coords.latitude;
    let longitud = data.coords.longitude;
    let position= new google.maps.LatLng(latitud, longitud);
    var iconBase = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/library_maps.png';
    var marker = new google.maps.Marker({
        position: position,
        icon: iconBase,
        map: map,
    });
}

