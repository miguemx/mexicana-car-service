$(document).ready(function(){
    $("#login-submite").click( login );
});

function login() {
    var login = new Object();
    login.username = $("#username").val();
    login.password = $("#password").val();
    login.type = "SYS";
    $.ajax({
        method: "post",
        url: BASEFRONT + "WebServices/loginService",
        data: JSON.stringify(login),
        dataType: 'json',
        beforeSend: function() {
            $("input, button").prop("disabled",true);
        },
        error: function(e) {
            $("input, button").prop("disabled",false);
        },
        success: function(response) {
            $("input, button").prop("disabled",false);
            if ( response.status == "ok" ) {
                var url = response.data.user.role.url;
                document.location = BASEFRONT+url;
            }
            else {
                alert ( response.data );
            }
        }
    });
}