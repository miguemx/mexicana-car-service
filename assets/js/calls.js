var writting = false; // flag used to say whether or nor is an input field being edited


function sendNewTrip(data) {
    var jsonData = JSON.stringify(data);
    var obj = new Object();
    obj.data = data;

    var json = JSON.stringify(obj);
    // conn.send(json);

    socket.emit('newcall', obj);

}


$(document).ready(function() {
    $("#btn-new-call").click(newCall);

    // each input's event on enter key
    $(':input').keyup(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            $(this).next(':input').focus();
        }
        if(keycode == '27'){
            cancelCall();
        }
    });

    // check if an input field is being edited
    $(':input').on( "focusin", function(){
        writting = true;
    })
    // check if an input field is NOT being edited
    $(':input').on( "focusout", function(){
        writting = false;
    })

    // catches the key pressed in the UI
    $(document).keyup(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if ( !writting ) {
            handleShortCuts(keycode)
        }
    });

    $("#btn-save-call").click(registerCall);

    getCalls();

    $(".call-row").click(function() {
        selectCall( $(this).prop('id') );
    });

});


/**
 * triggers an action according to key pressed on web interface
 * @param {int} keycode the keycode of the key pressed on web interface
 */
function handleShortCuts(keycode) {
    switch ( keycode ) {
        case 49: newCall(1); break;  // key 1  => new call from line 1
        case 50: newCall(2); break;  // key 2  => new call from line 2
        case 51: newCall(3); break;  // key 3  => new call from line 3
        case 52: newCall(4); break;  // key 4  => new call from line 4
        case 53: newCall(5); break;  // key 5  => new call from line 5
        case 54: newCall(6); break;  // key 6  => new call from line 6
        case 55: newCall(7); break;  // key 7  => new call from line 7
        case 56: newCall(8); break;  // key 8  => new call from line 8
        case 57: newCall(9); break;  // key 9  => new call from line 9
        case 48: newCall(10); break; // key 10 => new call from line 10
        case 110: 
        case 78: newCall(); break; // key N => new call without a line
        case 27: cancelCall(); break;
    }
}

/**
 * sets sumary of a service according to pickup and destination
 * @param {object} result result from a google maps route, which indicates eta, distance and route
 */
function setCallValues(result) {
    var milecost = 6;
    
    $("#txt-distance").val(result.routes[0].legs[0].distance.text);
    $("#txt-amount").val("6.00");
    $("#txt-eta").val(result.routes[0].legs[0].duration.text);
}

/**
 * stores a new call
 */
function registerCall() {
    var callData = $("#new-call-window input, #new-call-window select").serialize();
    clearNewCall();
    
    $.ajax({
        method: 'post',
        url: 'calls/add',
        data: callData,
        dataType: 'json',
        success: function ( data ) {
            if ( data.status == "ok" ) {
                sendNewTrip(data.data[0]);
                var html = getRowCallHTML(data.data[0]);
                initMap();
                //$(".table-calls tbody").prepend( html );
            }
        }
    });
    $("#new-call-window").hide();
}

/**
 * clears form for new call and hides window
 */
function cancelCall() {
    clearNewCall();
    $("#new-call-window").hide();
}

/**
 * creates a windows to create a new call.
 * @param {int} line (optional) line number
 */
function newCall(line) {
    clearNewCall();
    if ( line ) {
        $("#line").val(line);
        getPhoneFromLine(line);
        $("#span-incoming-call-name").html(' ON LINE '+line);
    }
    $("#new-call-window").show();
    if ( $("#phoneNumber").val().length < 1 ) {
        $("#area").focus();
    }
    else {
        $("#passengerName").focus();
    }
    // avisar al resto que ya se tomo la llamada
    var takeCall = new Object();
    takeCall.type = 'TAKECALL';
    takeCall.data = line;
    var json = JSON.stringify(takeCall);
    //conn.send(json);
    $("#txt-lines-"+line).val('');
}

function addCallFromOthers(data) {
    $(".table-calls tbody").prepend( getRowCallHTML(data) );
}

/**
 * clear form to register a new call
 */
function clearNewCall() {
    // limpiar todo el formulario
    $("#new-call-window input").val('');
    $("#span-incoming-call-name").html('');
    // initMap();
}

/**
 * get value from line input and parses to get the phone number
 * @param {int} line line number to obtain the phone number
 */
function getPhoneFromLine(line) {
    var phoneText = $("#txt-lines-" + line).val();
    var pattern = /^[0-9]+\-[0-9]+$/;
    if ( pattern.test(phoneText) ) {
        var arrayPhoneData = phoneText.split('-');
        $("#area").val(arrayPhoneData[0]);
        $("#phoneNumber").val(arrayPhoneData[1]);
    }

}


function getCalls() {
    $.ajax({
        method: 'get',
        url: 'calls/lista',
        dataType: 'json',
        success: function(data) {
            
            for(var i=0; i<data.length; i++) {
                
                $(".table-calls tbody").append( getRowCallHTML(data[i]) );
            }
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function getRowCallHTML(data) {
    var space = " ";
    
    var html =  '<tr class="call-row call-new" id="'+data.id+'">' +
                '<td>'+ data.daynumber +'</td>' +
                '<td>'+ data.pickupnumber + ' ' + data.pickupstreet +'</td>' +
                '<td>'+ data.destnumber + ' ' + data.deststreet +'</td>' +
                '<td>'+ data.line +'</td>' +
                '<td>'+ data.phone +'</td>' +
                '<td>'+ data.datecall +'</td>' +
                '<td>'+ space +'</td>' +
                '<td>'+ data.eta +'</td>' +
                '<td>'+ data.amount +'</td>' +
                '<td>'+ data.username +'</td>' +
                '<td>'+ ((data.vehicle)?data.vehicle:'') +'</td>' +
                '<td>'+ data.status +'</td>' +
    '</tr>';
    return html;
}

function selectCall(id) {
    $(".call-row").removeClass("call-row-selected");
    $("#"+id).addClass("call-row-selected");
}


function newNumberCall(number) {
    for ( var i=1; i<=16; i++ ) {
        var currVal = $("#txt-lines-"+i).val();
        if ( currVal.length == 0 ) {
            $("#txt-lines-"+i).val(number);
            break;
        }
    }
}

function removeNumber(line) {
    $("#txt-lines-"+line).val('');
}


function asigna() {
    // 145 frederick street
    // domingo llega chofer 01
    // sistema asigna llamada

    // 145 frederick street
    // domingo llega chofer 01
    // el sistema espera otro chofer cercano
    // asigna chofer 02

    // 147 frederick street
    // domingo llega chofer 01
    // sistema asigna chofer 01

}