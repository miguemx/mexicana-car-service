<?php

require dirname(__DIR__) . '/vendor/autoload.php';

$socket = new \HemiFrame\Lib\WebSocket\WebSocket('localhost', 8080);
$socket->on("receive", function($client, $data) use($socket) {
});
$client = $socket->connect();
if ($client) {
    for ( $i=0; $i<=1000; $i++) {
        $number = rand(1000000,9999999);
        $socket->sendData($client, '{"type":"LINE","data":"914-'.$number.'"}');
        sleep(15);
    }
	
	$socket->disconnectClient($client);
}