<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="<?php echo BASEFRONT; ?>assets/img/logo.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo BASEFRONT; ?>assets/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo BASEFRONT; ?>assets/css/manager.css" />

    <!-- scripts -->
    <script type="text/javascript" src="<?php echo BASEFRONT; ?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo BASEFRONT; ?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo BASEFRONT; ?>assets/js/bootstrap.min.js"></script>

    <title>Management System - Mexicana Car Service</title>

    <script type="text/javascript">
        var BASEFRONT = '<?php echo BASEFRONT; ?>';
    </script>
</head>
<body>
    <script src="https://unpkg.com/react@16/umd/react.production.min.js" crossorigin></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js" crossorigin></script>
    <script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>


    <div><?php $this->load->view('manager/menu'); ?></div>

    <div class="container-fluid">
        <?php $this->load->view('manager/'.$view); ?>
    </div>

    
</body>
</html>