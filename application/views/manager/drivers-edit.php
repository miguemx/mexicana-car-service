<div class="row">
    <div class="col-sm-12">
        <form method="post" action="<?php echo BASEFRONT; ?>Drivers/add" id="formEdit">
            <div class="row button-container">
                <div class="col-sm-6"><strong>Driver Information</strong></div>
                <div class="col-sm-6 text-right">
                    <button class="btn btn-secondary btn-sm" type="button" id="btnPrev">Prev</button>
                    <button class="btn btn-secondary btn-sm" type="button" id="btnNext">Next</button>
                    <button class="btn btn-secondary btn-sm" type="button" id="btnSave">Save</button>
                </div>
            </div>
            
            <div class="card">
                <div class="card-body">
                    <?php $this->load->view('forms/driver'); ?>
                </div>
            </div>
            <div class="col-sm-6"><strong>Vehicle Information</strong></div>
            <div class="card">
                <div class="card-body">
                    <?php $this->load->view('forms/vehicle'); ?>
                </div>
            </div>
        </form>
    </div>
</div> 
<div id="root"></div>

<script src="<?php echo base_url('assets/js/drivers.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/drivers-edit.js'); ?>" type="text/babel"></script>