<nav class="navbar navbar-expand-sm navbar-light background-menu">
    <a class="navbar-brand" href="<?php echo BASEFRONT; ?>dispatcher">
        <img src="<?php echo BASEFRONT; ?>assets/img/logo.png" height="25" />
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            
            <li class="nav-item">
                <a class="nav-link" href="<?php echo BASEFRONT; ?>dispatcher">Service Manager</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Drivers
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?php echo BASEFRONT; ?>Drivers/create">Add</a>
                    <a class="dropdown-item" href="<?php echo BASEFRONT; ?>Drivers">List</a>
                </div>
            </li>
            <!-- 
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Cars
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?php echo BASEFRONT; ?>Cars/new">Add</a>
                    <a class="dropdown-item" href="<?php echo BASEFRONT; ?>Cars">List</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Payments
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?php echo BASEFRONT; ?>Payments">Vehicles</a>
                    <a class="dropdown-item" href="<?php echo BASEFRONT; ?>Payments">Drivers</a>
                    <a class="dropdown-item" href="<?php echo BASEFRONT; ?>Payments">Payments</a>
                </div>
            </li> -->
            <li class="nav-item">
                <a class="nav-link disabled" href="<?php echo BASEFRONT; ?>man/Settings" tabindex="-1" aria-disabled="true">Settings</a>
            </li>
        </ul>
        <!-- <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2 form-control-sm" type="search" placeholder="Search drivers" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0 btn-sm" type="submit">Search</button>
        </form> -->
    </div>
</nav>

<style>
    .background-menu {
        background: #edffe3;
        border-bottom: 1px solid #aaa;
    }
</style>