<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-sm-6"><strong>Drivers List</strong></div>
                    <div class="col-sm-6 text-right">
                        <a href="<?php echo BASEFRONT; ?>Drivers/new" class="btn btn-sm btn-secondary">Add</a>
                    </div>
                </div>
                
            </div>
            <div class="card-body">
                <div id="root"></div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/drivers-list.js'); ?>" type="text/babel"></script>