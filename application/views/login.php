<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title>Welcome to Mexicana Car Service - Main System</title>
    <link rel="icon" type="image/png" href="<?php echo BASEFRONT; ?>assets/img/logo.png">
    <link rel="stylesheet" type="text/css" href="<?php echo BASEFRONT; ?>assets/css/login.css" />
    
    <script src="<?php echo BASEFRONT; ?>assets/js/jquery.js"></script>
    <script src="<?php echo BASEFRONT; ?>assets/js/jquery-ui.js"></script>

    <script type="text/javascript">
        var BASEFRONT = "<?php echo BASEFRONT; ?>";
    </script>
    <script src="<?php echo BASEFRONT; ?>assets/js/login.js"></script>

</head>
<body>

    <div class="main-container">
        <form method="post" action="<?php echo BASEFRONT; ?>Home/Login">
            <div class="login-container">
                <div class="brand-logo-container">
                    <img src="<?php echo BASEFRONT; ?>assets/img/logo.png" alt="Mexicana Car Service" class="" /> 
                </div>
                <div class="login-header">
                    Service Manager System
                </div>
                <div class="login-body">
                    <label>Username</label>
                    <input type="text" name="username" id="username" placeholder="Your username here" />

                    <label>Password</label>
                    <input type="password" name="password" id="password" placeholder="Your password here"  />
                </div>
                <div class="login-footer">
                    <button type="submit" id="login-submit">Login</button>
                </div>
                <div class="error-login"><?php echo $message; ?></div>
            </div>
        </form>
    </div>

</body>



</html>