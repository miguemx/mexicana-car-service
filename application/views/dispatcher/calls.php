<div class="table-calls-container">

    <div id="new-call-window">
        <?php include(__DIR__.'/newcallwindow.php'); ?>
    </div>

    <table class="table-calls">
        <thead>
            <tr>
                <td>Call #</td>
                <td>Pickup</td>
                <td>Destination</td>
                <td>Line</td>
                <td>Phone</td>
                <td>Time</td>
                <td>Out</td>
                <td>ETA</td>
                <td>AMT</td>
                <td>Oper.</td>
                <td>Car</td>
                <td>Status</td>
            </tr>
        </thead>

        <tbody>
            
        </tbody>
    </table>

</div>