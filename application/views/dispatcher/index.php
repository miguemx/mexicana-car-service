<html>
    <head>
        <title>Service Manager - Mexicana Car Service</title>
        <link rel="icon" type="image/png" href="<?php echo BASEFRONT; ?>assets/img/logo.png">

        <link rel="stylesheet" type="text/css" href="<?php echo BASEFRONT; ?>assets/css/dispatcher-layout.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASEFRONT; ?>assets/css/dispatcher-calls.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASEFRONT; ?>assets/css/dispatcher-lines.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASEFRONT; ?>assets/css/dispatcher-menu.css" />

        <script src="<?php echo BASEFRONT; ?>assets/js/socket.io.js"></script>
        <script src="<?php echo BASEFRONT; ?>assets/js/jquery.js"></script>
        <script src="<?php echo BASEFRONT; ?>assets/js/jquery-ui.js"></script>

        <script>
        const BASEURL = '<?php echo base_url(); ?>';
        </script>

    </head>

    <body>
        <div class="app-container" id="karla">
            <div class="app-brand-container">
                <?php $this->load->view('dispatcher/brand'); ?>
            </div> 
            <div class="app-menu-container">
                <?php $this->load->view('dispatcher/menu'); ?>
            </div>
            
            <div class="app-lines-container">
                <?php $this->load->view('dispatcher/lines'); ?>
            </div>
            <div class="app-messages-container">
                messages
            </div>
            <div class="app-status-container">
                status
            </div>
            <div class="app-calls-container">
                <?php $this->load->view('dispatcher/calls'); ?>
            </div>
            <div class="app-map-container" id="map">
                map
            </div>

            <div class="app-shortcuts-container"> 
                <?php $this->load->view('dispatcher/shortcuts'); ?>
            </div>
            <div class="app-mapcontrols-container">
                <?php $this->load->view('dispatcher/mapcontrols'); ?>
            </div>
        </div>

        <script src="<?php echo BASEFRONT; ?>assets/js/map.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCrxXLnGg2BkZn8oxS0b6dgwe5zB8RnyU&callback=initMap&libraries=places" async defer></script>

        <script src="<?php echo BASEFRONT; ?>assets/js/realtime.js"></script>
        <script src="<?php echo BASEFRONT; ?>assets/js/calls.js"></script>


    </body>
</html>