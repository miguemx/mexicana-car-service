<div class="main-menu-container">
    <ul class="main-menu">
        <li class="main-menu-item">
            <a href="<?php echo BASEFRONT; ?>man" id="menuLink1" class="main-menu-item-submenu">Management</a>
            <ul class="main-menu-submenu">
                <li class="main-menu-subitem"><a href="#" class="">Drivers</a></li>
                <li class="main-menu-subitem"><a href="#" class="">Cars</a></li>
                <li class="main-menu-subitem"><a href="#" class="">Operators</a></li>
                <li class="main-menu-subitem"><a href="#" class="">Users</a></li>
            </ul>
        </li>
        <li class="main-menu-item"><a href="<?php echo BASEFRONT ?>Home/Logout" class="">Log out</a></li>
    </ul>
</div>