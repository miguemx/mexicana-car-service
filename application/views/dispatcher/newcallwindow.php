<form>
    <div class="newcall-title">
        Incoming Call <span id="span-incoming-call-name"></span>
    </div>

    <table width="100%" cellpadding="5">
        <tr>
            <td width="40" valign="top">
                <label for="">Area</label><br />
                <input type="text" name="area" id="area" placeholder="000" maxlength="3" tabindex="1" />
            </td>
            <td width="100" valign="top">
                <label for="">Phone</label><br />
                <input type="text" name="phoneNumber" id="phoneNumber" placeholder="0000000" maxlength="10" tabindex="2" />
                <input type="hidden" name="phone" id="phone" />
            </td>
            <td width="" valign="top">
                <label for="">Passenger Name</label><br />
                <input type="text" name="passengerName" id="passengerName" placeholder="John Smith" maxlength="60" tabindex="3" />
            </td>
            <td width="40" valign="top">
                <label for="">Line</label><br />
                <input type="text" name="line" id="line" placeholder="00" maxlength="2" tabindex="4" />
            </td>
            <td width="200" valign="top" class="newcall-sumary" rowspan="2">
                <label>Distance:</label> <input type="text" name="distance" id="distance" readonly="readonly" /> <br />
                <label>ETA:</label> <input type="text" name="eta" id="eta" readonly="readonly" /> <br />
                <label>Amount:</label> <input type="text" name="amount" id="amount" readonly="readonly" style="font-weight: bold;" /> <br />
            </td>
        </tr>

        <tr>
            <td colspan="4" valign="top">
                <label for="">Pick-up Address</label><br />
                <input type="text" name="pickupAddress" id="pickupAddress" placeholder="000 Streetname St, City, State" class="maps-autocomplete" tabindex="5" />
            </td>
        </tr>

        <tr>
            <td colspan="4" valign="top">
                <label for="">Destination</label><br />
                <input type="text" name="destinationAddress" id="destinationAddress" placeholder="000 Streetname St, City, State" class="maps-autocomplete" tabindex="6" />
            </td>
            <td valign="top">
                <label>Car type</label><br />
                <select id="carType" name="carType" tabindex="7">
                    <option value="1">Sedan</option>
                    <option value="2">Minivan</option>
                    <option value="3">SUV</option>
                </select>
            </td>
        </tr>

        <tr>
            <td colspan="5">
                
                <label>Waypoints</label>
                <button type="button" onclick="addWayPoint()">Mas</button>
                <div id="newcall-waypoints-inputs">
                    <input type="text" class="txt-waypoint maps-autocomplete" onblur="calculateRoute()" />
                </div>
                
            </td>
        </tr>
    </table>

    <div class="newcall-tools">
        <button type="button" id="btn-save-call" tabindex="7">Save</button>
    </div>
    <input type="text" id="pickup" name="pickup" />
    <input type="text" id="destination" name="destination" />
</form>