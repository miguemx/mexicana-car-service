<div class="brand-logo-container">
    <img src="<?php echo BASEFRONT; ?>assets/img/logo.png" alt="Mexicana Car Service" class="" /> 
</div>
<div class="brand-app-name">
    Service Manager
</div>

<style>
.brand-logo-container {
    float: left;
}

.brand-logo-container img {
    max-height: 25px;
}

.brand-app-name {
    float: left;
    margin-left: 10px;
    font-weight: bold;
}

.brand-user-id {
    font-weight: normal;

}
</style>