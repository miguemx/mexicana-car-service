
<input type="hidden" id="personId" value="" />
<div class="form-row">
    <div class="form-group col-sm-2">
        <label for="">Driver Num.</label>
        <input type="text" class="form-control form-control-sm" id="driverId" placeholder="0000" value="<?php echo $driverId; ?>" />
    </div>
    <div class="form-group col-sm-2">
        <label for="">S.S. Num.</label>
        <input type="text" class="form-control form-control-sm" id="ssNumber" placeholder="000000000" />
    </div>
    <div class="form-group col-sm-2">
        <label for="">Status</label>
        <select class="form-control form-control-sm" id="driverStatus">
            <option value="ACTIVE">ACTIVE</option>
            <option value="HOLD">HOLD</option>
            <option value="INACTIVE">INACTIVE</option>
            <option value="VACATION">VACATION</option>
            <option value="BACKUP">BACKUP</option>
            <option value="RETIRED">RETIRED</option>
        </select>
    </div>
    <div class="form-group col-sm-2">
        <label for="">Home Phone</label>
        <input type="text" class="form-control form-control-sm" id="phone" placeholder="9140000000" />
    </div>
    <div class="form-group col-sm-2">
        <label for="">D.O.B.</label>
        <input type="date" class="form-control form-control-sm" id="birth" placeholder="00/00/0000" />
    </div>
    <div class="form-group col-sm-2">
        <label for="">Begin Date</label>
        <input type="date" class="form-control form-control-sm" id="beginDate" placeholder="9140000000" />
    </div>
    
</div>

<div class="form-row">
    <div class="form-group col-sm-3">
        <label for="">Givenname</label>
        <input type="text" class="form-control form-control-sm" id="givenname" placeholder="John" />
    </div>
    <div class="form-group col-sm-3">
        <label for="">Surname</label>
        <input type="text" class="form-control form-control-sm" id="surname" placeholder="Smith" />
    </div>
    <div class="form-group col-sm-3">
        <label for="">Cellular</label>
        <input type="text" class="form-control form-control-sm" id="cellphone" placeholder="9140000000" />
    </div>
    <div class="form-group col-sm-3">
        <label for="">US Citizen</label>
        <input type="checkbox" class="form-control form-control-sm" id="usCitizen" value="1" />
    </div>
</div>

<div class="form-row">
    <div class="form-group col-sm-6">
        <label>Address</label>
        <input type="text" class="form-control form-control-sm" id="address" placeholder="321 Name Street" />
    </div>
    <div class="form-group col-sm-2">
        <label>City</label>
        <input type="text" class="form-control form-control-sm" id="city" placeholder="Yonkers" />
    </div>
    <div class="form-group col-sm-2">
        <label>State</label>
        <input type="text" class="form-control form-control-sm" id="state" placeholder="NY"  />
    </div>
    <div class="form-group col-sm-2">
        <label>ZIP</label>
        <input type="text" class="form-control form-control-sm" id="zip" placeholder="12345"  />
    </div>
</div>

<div class="form-row">
    <div class="form-group col-sm-1">
        <label>DRVDN</label>
        <input type="text" class="form-control form-control-sm" id="drvn" placeholder="5555"  />
    </div>
    <div class="form-group col-sm-2">
        <label>Licence Num.</label>
        <input type="text" class="form-control form-control-sm" id="licence" placeholder="123456789"  />
    </div>
    <div class="form-group col-sm-1">
        <label> Type</label>
        <input type="text" class="form-control form-control-sm" id="licenceType" placeholder="e" />
    </div>
    <div class="form-group col-sm-2">
        <label>Licence Exp. Date</label>
        <input type="date" class="form-control form-control-sm" id="licenceExpDate" placeholder="00/00/0000"  />
    </div>
    <div class="form-group col-sm-2">
        <label>TLC Lic</label>
        <input type="text" class="form-control form-control-sm" id="tlcLic" placeholder="1234" />
    </div>
    <div class="form-group col-sm-2">
        <label>TLC Exp. Date</label>
        <input type="date" class="form-control form-control-sm" id="tlcExpDate" placeholder="00/00/0000"  />
    </div>
    <div class="form-group col-sm-2">
        <label>TLC Urine Exp. Date</label>
        <input type="date" class="form-control form-control-sm" id="tlcUrineExp" placeholder="00/00/0000"  />
    </div>
</div>
