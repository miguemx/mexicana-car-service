<input type="hidden" id="carId" value="" />
<div class="form-row">
    <div class="form-group col-sm-2">
        <label for="">Plate</label>
        <input type="text" class="form-control form-control-sm" id="plate" placeholder="1234AB" />
    </div>
    <div class="form-group col-sm-2">
        <label for="">Model</label>
        <input type="text" class="form-control form-control-sm" id="model" placeholder="RAV4" />
    </div>
    <div class="form-group col-sm-2">
        <label for="">Make</label>
        <input type="text" class="form-control form-control-sm" id="make" placeholder="TOYOTA" />
    </div>
    <div class="form-group col-sm-2">
        <label for="">Type</label>
        <select class="form-control form-control-sm" id="carTypeId">
            <option value="1">SEDAN</option>
            <option value="2">MINIVAN</option>
            <option value="3">SUV</option>
        </select>
    </div>
    <div class="form-group col-sm-2">
        <label for="">Color</label>
        <select class="form-control form-control-sm" id="carColor">
            <option class="op-black" value="BLACK">BLACK</option>
            <option class="op-red" value="RED">RED</option>
            <option class="op-darkred" value="DARKRED">DARKRED</option>
            <option class="op-blue" value="BLUE">BLUE</option>
            <option class="op-darkblue" value="DARKBLUE">DARKBLUE</option>
            <option class="op-green" value="GREEN">GREEN</option>
            <option class="op-gray" value="GRAY">GRAY</option>
            <option class="op-white" value="WHITE">WHITE</option>
            <option class="op-yellow" value="YELLOW">YELLOW</option>
            <option class="op-silver" value="SILVER">SILVER</option>
            <option class="op-gold" value="GOLD">GOLD</option>
        </select>
    </div>
    <div class="form-group col-sm-2">
        <label for="">Reg. Exp. Date</label>
        <input type="date" class="form-control form-control-sm" id="regExpDate" placeholder="00/00/0000" />
    </div>    
</div>

<div class="form-row">
    <div class="form-group col-sm-1">
        <label for="">Year</label>
        <input type="text" class="form-control form-control-sm" id="year" placeholder="2010" />
    </div>
    <div class="form-group col-sm-4">
        <label for="">Ins. Company</label>
        <input type="text" class="form-control form-control-sm" id="insCompany" placeholder="Global Liberty" />
    </div>
    <div class="form-group col-sm-2">
        <label for="">Policy</label>
        <input type="text" class="form-control form-control-sm" id="insPolicy" placeholder="ABCDE12345-6" />
    </div>
    <div class="form-group col-sm-2">
        <label for="">Ins. Exp. Date</label>
        <input type="date" class="form-control form-control-sm" id="insExpDate" placeholder="00/00/0000" />
    </div>
    <div class="form-group col-sm-3">
        <label for="">V.I.</label>
        <input type="text" class="form-control form-control-sm" id="vI" placeholder="ABCDE12FG3H456789" />
    </div>
</div>

<div class="form-row">
    <div class="form-group col-sm-3">
        <label for="">T.L.C. Diamond</label>
        <input type="text" class="form-control form-control-sm" id="tlcDiamond" placeholder="0000" />
    </div>
    <div class="form-group col-sm-3">
        <label for="">Diamond Exp. Date</label>
        <input type="date" class="form-control form-control-sm" id="diamondExpDate" placeholder="00/00/0000" />
    </div>
    <div class="form-group col-sm-3">
        <label for="">Inspection Date</label>
        <input type="date" class="form-control form-control-sm" id="inspectionDate" placeholder="00/00/0000" />
    </div>
    <div class="form-group col-sm-3">
        <label for="">Owned by company</label>
        <select class="form-control form-control-sm" id="owned">
            <option value="0">No</option>
            <option value="1">Yes</option>
        </select>
    </div>
</div>

<div class="form-row">
    <div class="form-group col-sm-12">
        <label for="">Registred Name</label>
        <input type="text" class="form-control form-control-sm" id="registredName" placeholder="john smith" />
    </div>
</div>