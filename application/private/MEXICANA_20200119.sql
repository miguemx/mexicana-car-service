-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 20, 2020 at 03:20 AM
-- Server version: 8.0.14
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `MEXICANA`
--
CREATE DATABASE IF NOT EXISTS `MEXICANA` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `MEXICANA`;

-- --------------------------------------------------------

--
-- Table structure for table `access_roles`
--

DROP TABLE IF EXISTS `access_roles`;
CREATE TABLE `access_roles` (
  `ROLE_ID` int(10) UNSIGNED NOT NULL,
  `ROLE_NAME` varchar(45) NOT NULL,
  `ROLE_URL` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access_roles`
--

INSERT INTO `access_roles` VALUES(1, 'ADMIN', 'dispatcher');
INSERT INTO `access_roles` VALUES(2, 'OPERATOR', 'dispatcher');
INSERT INTO `access_roles` VALUES(3, 'DRIVER', 'profile');

-- --------------------------------------------------------

--
-- Table structure for table `access_users`
--

DROP TABLE IF EXISTS `access_users`;
CREATE TABLE `access_users` (
  `USER_ID` int(10) UNSIGNED NOT NULL,
  `USER_ROLE` int(10) UNSIGNED NOT NULL,
  `USER_PERSON` int(10) UNSIGNED NOT NULL,
  `USER_USERNAME` varchar(30) NOT NULL,
  `USER_PASSWORD` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access_users`
--

INSERT INTO `access_users` VALUES(1, 1, 2, 'jonathan', '0246874114a63ee156cb1e6840ea0baee117ad8a');
INSERT INTO `access_users` VALUES(2, 2, 3, 'miguel', '0246874114a63ee156cb1e6840ea0baee117ad8a');

-- --------------------------------------------------------

--
-- Table structure for table `core_calls`
--

DROP TABLE IF EXISTS `core_calls`;
CREATE TABLE `core_calls` (
  `CALL_ID` int(10) UNSIGNED NOT NULL,
  `CALL_OPERATOR` int(10) UNSIGNED DEFAULT NULL,
  `CALL_CAR_TYPE` int(10) UNSIGNED NOT NULL,
  `CALL_CLIENT` int(10) UNSIGNED DEFAULT NULL,
  `CALL_DAY_NUMBER` int(10) UNSIGNED NOT NULL,
  `CALL_LINE` varchar(3) DEFAULT NULL,
  `CALL_DATE_TIME` datetime NOT NULL,
  `CALL_TIMESTAMP` int(10) UNSIGNED NOT NULL,
  `CALL_PHONE` varchar(15) DEFAULT NULL,
  `CALL_STATUS` enum('NEW','CANCELLED','DISPATCHED','ON TRIP','COMPLETED') NOT NULL,
  `CALL_DISPATCH_DATE_TIME` datetime DEFAULT NULL,
  `CALL_DISPATCH_TIMESTAMP` int(11) DEFAULT NULL,
  `CALL_DISPATCH_TYPE` enum('DRIVER','DISPATCHER','AUTO') DEFAULT NULL,
  `CALL_REGISTER_TYPE` enum('CALL','OFFICE','APP') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `core_calls`
--

INSERT INTO `core_calls` VALUES(1, 1, 1, NULL, 1, '6', '2019-09-22 02:16:57', 1569118617, '9142342342342', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(2, 1, 1, NULL, 2, '1', '2019-09-22 03:11:40', 1569121900, '91423232323', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(3, 1, 1, NULL, 1, '5', '2019-10-06 00:45:36', 1570322736, '914234234', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(4, 1, 1, NULL, 2, '2', '2019-10-06 00:46:26', 1570322786, '9142342342', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(5, 1, 1, NULL, 3, '1', '2019-10-06 00:50:17', 1570323017, '913324222', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(6, 1, 1, NULL, 4, '2', '2019-10-06 00:54:11', 1570323251, '913w234234', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(7, 1, 1, NULL, 5, '7', '2019-10-06 01:16:59', 1570324619, '9133422222', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(8, 1, 1, NULL, 6, '7', '2019-10-06 01:18:46', 1570324726, '912131231231', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(9, 1, 1, NULL, 1, '9', '2019-10-08 03:04:21', 1570503861, '914234234234', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(10, 1, 1, NULL, 2, '7', '2019-10-08 03:16:35', 1570504595, '914234234', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(11, 1, 1, NULL, 3, '7', '2019-10-08 03:22:36', 1570504956, '91423423', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(12, 1, 1, NULL, 4, '5', '2019-10-08 03:42:24', 1570506144, '91498987', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(13, 1, 1, NULL, 1, '9', '2019-10-26 17:16:51', 1572110211, '9149898989', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(14, 1, 1, NULL, 1, '6', '2019-10-27 00:26:37', 1572135997, '91387687888', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(15, 1, 1, NULL, 2, '8', '2019-10-27 00:58:16', 1572137896, '91312323232', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(16, 1, 1, NULL, 3, '9', '2019-10-27 03:29:28', 1572146968, '913123123', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(17, 1, 1, NULL, 4, '2', '2019-10-27 03:35:20', 1572147320, '123123123', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(18, 1, 1, NULL, 5, '8', '2019-10-27 03:43:01', 1572147781, '91191111', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(19, 1, 1, NULL, 6, '88', '2019-10-27 03:48:29', 1572148109, '828828', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(20, 1, 1, NULL, 7, '88', '2019-10-27 03:49:57', 1572148197, '88888', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(21, 1, 1, NULL, 8, '7', '2019-10-27 03:51:50', 1572148310, '1212121', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(22, 1, 1, NULL, 9, '6', '2019-10-27 03:52:11', 1572148331, '223333333', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(23, 1, 1, NULL, 10, '1', '2019-10-27 03:58:40', 1572148720, '1212', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(24, 1, 1, NULL, 11, '5', '2019-10-27 04:03:41', 1572149021, '1212', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(25, 1, 1, NULL, 12, '6', '2019-10-27 04:12:55', 1572149575, '6666', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(26, 1, 1, NULL, 1, '', '2020-01-03 03:50:25', 1578023425, '914423234', 'NEW', NULL, NULL, NULL, 'CALL');
INSERT INTO `core_calls` VALUES(27, 1, 1, NULL, 2, '88', '2020-01-03 17:35:39', 1578072939, '914993939393', 'NEW', NULL, NULL, NULL, 'CALL');

-- --------------------------------------------------------

--
-- Table structure for table `core_car_types`
--

DROP TABLE IF EXISTS `core_car_types`;
CREATE TABLE `core_car_types` (
  `CARTYPE_ID` int(10) UNSIGNED NOT NULL,
  `CARTYPE_NAME` varchar(45) NOT NULL,
  `CARTYPE_PASS_NUMBER` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `core_car_types`
--

INSERT INTO `core_car_types` VALUES(1, 'SEDAN', 4);
INSERT INTO `core_car_types` VALUES(2, 'MINIVAN', 4);
INSERT INTO `core_car_types` VALUES(3, 'SUV', 7);

-- --------------------------------------------------------

--
-- Table structure for table `core_places`
--

DROP TABLE IF EXISTS `core_places`;
CREATE TABLE `core_places` (
  `PLACE_ID` int(10) UNSIGNED NOT NULL,
  `PLACE_MAPS_ID` varchar(45) NOT NULL,
  `PLACE_MAPS_COORD` varchar(200) NOT NULL,
  `PLACE_MAPS_NAME` varchar(250) NOT NULL,
  `PLACE_NUMBER` varchar(15) DEFAULT NULL,
  `PLACE_STREET` varchar(80) DEFAULT NULL,
  `PLACE_CITY` varchar(80) DEFAULT NULL,
  `PLACE_STATE` varchar(80) DEFAULT NULL,
  `PLACE_ZIP` varchar(15) DEFAULT NULL,
  `PLACE_COUNTRY` varchar(65) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `core_places`
--

INSERT INTO `core_places` VALUES(1, 'ChIJq9VxW3vywokRa7m2Y5WA4eY', '40.930903,-73.8954578', 'Mexicana Car Service', '226', 'New Main Street', 'Yonkers', 'New York', '10701', 'Estados Unidos');
INSERT INTO `core_places` VALUES(2, 'ChIJcWnnWiz0wokRCB6aVdnDQEk', '40.8296426,-73.9261745', 'Yankee Stadium', '1', 'East 161 Street', 'Bronx County', 'New York', '10451', 'Estados Unidos');
INSERT INTO `core_places` VALUES(3, 'ChIJaXQRs6lZwokRY6EFpJnhNNE', '40.7484405,-73.9856644', 'Empire State Building', '20', 'West 34th Street', 'New York', 'New York', '10001', 'Estados Unidos');
INSERT INTO `core_places` VALUES(4, 'ChIJhRwB-yFawokR5Phil-QQ3zM', '40.7505045,-73.9934387', 'Madison Square Garden', '4', 'Pennsylvania Plaza', 'New York', 'New York', '10001', 'Estados Unidos');
INSERT INTO `core_places` VALUES(5, 'ChIJYaVdffBYwokRnTOoCzCq9mE', '40.7644691,-73.9744877', 'Hotel Plaza', '768', '5th Avenue', 'New York', 'New York', '10019', 'Estados Unidos');
INSERT INTO `core_places` VALUES(6, 'ChIJPS8b1vhYwokRldqq2YHmxJI', '40.759976,-73.9799772', 'Radio City Music Hall', '1260', '6th Avenue', 'New York', 'New York', '10020', 'Estados Unidos');
INSERT INTO `core_places` VALUES(7, 'ChIJo_pSQQJZwokRxmR0JuOljmY', '40.7522669,-73.9749494', 'Mc Donald Kristen', '425', 'Lexington Avenue', 'New York', 'New York', '10017', 'Estados Unidos');
INSERT INTO `core_places` VALUES(8, 'ChIJRcvoOxpawokR7R4dQMXMMPQ', '40.7114743,-74.0134432', '9/11 Memorial', '180', 'Greenwich Street', 'New York', 'New York', '10007', 'Estados Unidos');
INSERT INTO `core_places` VALUES(9, 'ChIJtTmZ1wVfwokRDRBhLIcxjHI', '40.750561,-73.895051', 'Mexicana Car & Limo Services', '70-18', '35th Avenue', 'Queens County', 'New York', '11372', 'Estados Unidos');
INSERT INTO `core_places` VALUES(10, 'ChIJR0lA1VBmwokR8BGfSBOyT-w', '40.6413111,-73.7781391', 'Aeropuerto Internacional John F. Kennedy', '', '', 'Queens', 'Nueva York', '11430', 'Estados Unidos');
INSERT INTO `core_places` VALUES(11, 'ChIJrc9T9fpYwokRdvjYRHT8nI4', '40.7624284,-73.973794', 'Trump Tower', '725', '5th Avenue', 'New York', 'New York', '10022', 'Estados Unidos');
INSERT INTO `core_places` VALUES(12, 'ChIJE_cwOnHtwokRfc4HnIAUE4w', '40.951155,-73.8821743', '145 Frederic St', '145', 'Frederic Street', 'Yonkers', 'New York', '10703', 'Estados Unidos');
INSERT INTO `core_places` VALUES(13, 'ChIJXT2YXHvywokR0nRwN7b_pjU', '40.9309014,-73.8954496', '226 New Main St', '226', 'New Main Street', 'Yonkers', 'New York', '10701', 'Estados Unidos');
INSERT INTO `core_places` VALUES(14, 'ChIJQ6wr2nntwokR6iztQuquiHg', '40.9479161,-73.8850683', '160 Voss Ave', '160', 'Voss Avenue', 'Yonkers', 'New York', '10703', 'Estados Unidos');
INSERT INTO `core_places` VALUES(15, 'ChIJN8jPXzqTwokR3q14Rb0C58w', '40.9814708,-73.827699', 'Best Buy', '2478', 'Central Park Avenue', 'Yonkers', 'New York', '10710', 'Estados Unidos');
INSERT INTO `core_places` VALUES(16, 'ChIJbVfZzhBawokRnmCT-muojBA', '40.7080624,-74.0121849', 'Iglesia de la Trinidad', '75', 'Broadway', 'New York', 'New York', '10006', 'Estados Unidos');
INSERT INTO `core_places` VALUES(17, 'ChIJrx3puJtfwokRyqz0WNsg0ts', '40.7670052,-73.8811159', '88-08 23rd Ave', '88-08', '23rd Avenue', 'Queens County', 'New York', '11369', 'Estados Unidos');
INSERT INTO `core_places` VALUES(18, 'ChIJhzhIHL7ywokRDk7vsCNQ5tw', '40.9194178,-73.8647338', '810 Yonkers Ave', '810', 'Yonkers Avenue', 'Yonkers', 'New York', '10704', 'Estados Unidos');
INSERT INTO `core_places` VALUES(19, 'ChIJ7c0agMDzwokRQ1J6gtJKe_E', '40.885751,-73.91253', '3260 Henry Hudson Pkwy', '3260', 'Henry Hudson Parkway', 'Bronx County', 'New York', '10463', 'Estados Unidos');
INSERT INTO `core_places` VALUES(20, 'ChIJh7tBHr7ywokR4hFTwLzlguk', '40.9193015,-73.8645107', 'Empire City Casino', '810', 'Yonkers Avenue', 'Yonkers', 'New York', '10704', 'Estados Unidos');
INSERT INTO `core_places` VALUES(21, 'ChIJD66RLwX0wokRA0YqPmYIAiE', '40.8558937,-73.9260333', '549 Audubon Ave', '549', 'Audubon Avenue', 'New York', 'New York', '10040', 'Estados Unidos');
INSERT INTO `core_places` VALUES(22, 'ChIJDb303mTywokRXGCAKpPAZgI', '40.9292927,-73.897861', '127 S Broadway', '127', 'South Broadway', 'Yonkers', 'New York', '10701', 'Estados Unidos');
INSERT INTO `core_places` VALUES(23, 'ChIJwVdxgWTywokRJmqlnXB7qsw', '40.9277875,-73.8950301', '33 Herriot St', '33', 'Herriot Street', 'Yonkers', 'New York', '10701', 'Estados Unidos');
INSERT INTO `core_places` VALUES(24, 'ChIJOzjDa3DywokRaf0dqLoVYtA', '40.934408,-73.9012525', '66 Main St', '66', 'Main Street', 'Yonkers', 'New York', '10701', 'Estados Unidos');

-- --------------------------------------------------------

--
-- Table structure for table `core_trips`
--

DROP TABLE IF EXISTS `core_trips`;
CREATE TABLE `core_trips` (
  `TRIP_ID` int(10) UNSIGNED NOT NULL,
  `TRIP_CALL` int(10) UNSIGNED NOT NULL,
  `TRIP_VEHICLE` varchar(4) DEFAULT NULL,
  `TRIP_PICKUP` int(10) UNSIGNED NOT NULL,
  `TRIP_DESTINATION` int(10) UNSIGNED NOT NULL,
  `TRIP_STATUS` enum('REQUESTED','ONGOING','COMPLETED','CANCELLED') NOT NULL DEFAULT 'REQUESTED',
  `TRIP_AMOUNT` float NOT NULL,
  `TRIP_ETA` varchar(10) NOT NULL,
  `TRIP_START_TIME` int(10) UNSIGNED DEFAULT NULL,
  `TRIP_END_TIME` int(10) UNSIGNED DEFAULT NULL,
  `TRIP_DISTANCE` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `core_trips`
--

INSERT INTO `core_trips` VALUES(1, 1, NULL, 1, 2, 'REQUESTED', 15.05, '20 min', NULL, NULL, 10);
INSERT INTO `core_trips` VALUES(2, 2, NULL, 1, 3, 'REQUESTED', 22.41, '39 min', NULL, NULL, 17.4);
INSERT INTO `core_trips` VALUES(3, 3, NULL, 1, 2, 'REQUESTED', 15.05, '20 min', NULL, NULL, 10);
INSERT INTO `core_trips` VALUES(4, 4, NULL, 3, 2, 'REQUESTED', 13.31, '24 min', NULL, NULL, 8.3);
INSERT INTO `core_trips` VALUES(5, 5, NULL, 4, 5, 'REQUESTED', 6.6, '12 min', NULL, NULL, 1.6);
INSERT INTO `core_trips` VALUES(6, 6, NULL, 6, 7, 'REQUESTED', 5.79, '8 min', NULL, NULL, 0.8);
INSERT INTO `core_trips` VALUES(7, 7, NULL, 8, 3, 'REQUESTED', 8.46, '22 min', NULL, NULL, 3.5);
INSERT INTO `core_trips` VALUES(8, 8, NULL, 9, 10, 'REQUESTED', 29.98, '39 min', NULL, NULL, 25);
INSERT INTO `core_trips` VALUES(9, 9, NULL, 5, 11, 'REQUESTED', 5.16, '2 min', NULL, NULL, 0.2);
INSERT INTO `core_trips` VALUES(10, 10, NULL, 12, 13, 'REQUESTED', 7.05, '8 min', NULL, NULL, 2.1);
INSERT INTO `core_trips` VALUES(11, 11, NULL, 14, 12, 'REQUESTED', 5.32, '1 min', NULL, NULL, 0.3);
INSERT INTO `core_trips` VALUES(12, 12, NULL, 1, 15, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(13, 13, NULL, 1, 2, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(14, 14, NULL, 3, 8, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(15, 15, NULL, 1, 2, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(16, 16, NULL, 6, 5, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(17, 17, NULL, 4, 3, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(18, 18, NULL, 8, 16, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(19, 19, NULL, 17, 1, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(20, 20, NULL, 18, 2, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(21, 21, NULL, 19, 20, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(22, 22, NULL, 21, 5, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(23, 23, NULL, 22, 1, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(24, 24, NULL, 23, 1, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(25, 25, NULL, 24, 1, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(26, 26, NULL, 1, 2, 'REQUESTED', 0, '', NULL, NULL, 0);
INSERT INTO `core_trips` VALUES(27, 27, NULL, 1, 3, 'REQUESTED', 0, '', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `core_trip_stops`
--

DROP TABLE IF EXISTS `core_trip_stops`;
CREATE TABLE `core_trip_stops` (
  `TRIPSTOP_ID` int(10) UNSIGNED NOT NULL,
  `TRIPSTOP_PLACE` int(10) UNSIGNED NOT NULL,
  `TRIPSTOP_TRIP` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `core_vehicles`
--

DROP TABLE IF EXISTS `core_vehicles`;
CREATE TABLE `core_vehicles` (
  `VEHICLE_ID` varchar(4) NOT NULL,
  `VEHICLE_TYPE` int(10) UNSIGNED NOT NULL,
  `VEHICLE_PLATE` varchar(12) NOT NULL,
  `VEHICLE_YEAR` varchar(4) NOT NULL,
  `VEHICLE_MODEL` varchar(45) NOT NULL,
  `VEHICLE_MAKE` varchar(45) NOT NULL,
  `VEHICLE_COLOR` varchar(15) NOT NULL,
  `VEHICLE_OWNED` varchar(1) NOT NULL,
  `VEHICLE_REG_EXP_DATE` date DEFAULT NULL,
  `VEHICLE_INS_POLICY` varchar(45) DEFAULT NULL,
  `VEHICLE_INS_EXP_DATE` date DEFAULT NULL,
  `VEHICLE_INS_COMPANY` varchar(45) DEFAULT NULL,
  `VEHICLE_V_I` varchar(45) DEFAULT NULL,
  `VEHICLE_TLC_DIAMOND` varchar(45) DEFAULT NULL,
  `VEHICLE_DIAMOND_EXP_DATE` date DEFAULT NULL,
  `VEHICLE_INSPECTION_DATE` date DEFAULT NULL,
  `VEHICLE_REGISTRED_NAME` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `core_vehicles`
--

INSERT INTO `core_vehicles` VALUES('1001', 1, 'UAZ8146', '2017', 'SONIC', 'CHEVROLET', 'GRAY', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_vehicles` VALUES('1002', 1, 'PLATE', '1999', 'MODEL', 'MAKE', 'BLACK', '0', '2019-09-02', 'POLICY', '2019-09-18', 'COMPANY', 'VIVIVIVIVIVIV', 'DIAOS', '2019-09-11', '2019-09-16', '');
INSERT INTO `core_vehicles` VALUES('1003', 1, '123123123', '2020', 'MODEL', 'MAJE', 'BLACK', '0', '2019-09-12', 'DDDDDDDDD', '2019-09-20', 'SDFSDFSDFSDFSDF ', 'DSFS FSDFASDFA SDFADS FASDF', 'ASDASDASDASDASD', '2019-09-12', '2019-09-13', 'ASDASDASDASD');
INSERT INTO `core_vehicles` VALUES('1004', 1, '444AB555', '2014', 'RAV4', 'TOYOTA', 'BLACK', '0', '2020-01-07', '444444', '2020-01-15', 'GLOBAL LIBERTY', 'KJHKSDFJ44444444', '4444', '2020-01-30', '2020-01-16', 'REGISTREED FOUR');
INSERT INTO `core_vehicles` VALUES('1005', 1, '', '', '', '', 'BLACK', '0', '0000-00-00', '', '0000-00-00', '', '', '', '0000-00-00', '0000-00-00', '');
INSERT INTO `core_vehicles` VALUES('1006', 1, '', '', '', '', 'BLACK', '0', '0000-00-00', '', '0000-00-00', '', '', '', '0000-00-00', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `management_clients`
--

DROP TABLE IF EXISTS `management_clients`;
CREATE TABLE `management_clients` (
  `CLIENT_ID` int(10) UNSIGNED NOT NULL,
  `CLIENT_PERSON` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `management_drivers`
--

DROP TABLE IF EXISTS `management_drivers`;
CREATE TABLE `management_drivers` (
  `DRIVER_ID` int(10) UNSIGNED NOT NULL,
  `DRIVER_CAR` varchar(4) NOT NULL,
  `DRIVER_PERSON` int(10) UNSIGNED NOT NULL,
  `DRIVER_IMEI` varchar(45) NOT NULL,
  `DRIVER_PIN` varchar(4) NOT NULL,
  `DRIVER_STATUS` enum('ACTIVE','HOLD','INACTIVE','VACATION','BACKUP','RETIRED') NOT NULL,
  `DRIVER_ONLINE` varchar(1) NOT NULL DEFAULT '0',
  `DRIVER_OWNER` varchar(1) NOT NULL DEFAULT '0',
  `DRIVER_DRVDN` varchar(4) DEFAULT NULL,
  `DRIVER_LICENCE` varchar(45) NOT NULL,
  `DRIVER_LICENCE_TYPE` varchar(5) DEFAULT NULL,
  `DRIVER_LICENCE_EXP` date DEFAULT NULL,
  `DRIVER_TLC_LIC` varchar(4) DEFAULT NULL,
  `DRIVER_TLC_EXPDATE` date DEFAULT NULL,
  `DRIVER_BEGIN_DATE` date DEFAULT NULL,
  `DRIVER_TLC_URINE_EXP` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `management_drivers`
--

INSERT INTO `management_drivers` VALUES(1001, '1001', 4, '12403889', '3889', 'ACTIVE', '0', '0', NULL, 'MXNPUE2016', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `management_drivers` VALUES(1002, '1002', 15, '6464', '6464', 'ACTIVE', '1', '0', '6464', '9879879879879', 'F', '2019-09-11', 'TLCL', '2019-09-09', '2019-09-03', '2019-09-17');
INSERT INTO `management_drivers` VALUES(1003, '1003', 16, '2020', '2020', 'ACTIVE', '1', '0', '2020', '234EWFD34EWF', 'E', '2019-09-21', '2020', '2019-09-11', '2019-09-25', '2019-09-05');
INSERT INTO `management_drivers` VALUES(1004, '1004', 17, '4444', '4444', 'ACTIVE', '1', '0', '4444', '44444-44444', '4', '2020-01-23', '4444', '2020-01-15', '2020-01-08', '2020-01-22');
INSERT INTO `management_drivers` VALUES(1005, '1005', 18, '', '', 'ACTIVE', '1', '0', '', '', '', '0000-00-00', '', '0000-00-00', '0000-00-00', '0000-00-00');
INSERT INTO `management_drivers` VALUES(1006, '1006', 19, '', '', 'ACTIVE', '1', '0', '', '', '', '0000-00-00', '', '0000-00-00', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `management_persons`
--

DROP TABLE IF EXISTS `management_persons`;
CREATE TABLE `management_persons` (
  `PERSON_ID` int(10) UNSIGNED NOT NULL,
  `PERSON_SURNAME` varchar(45) NOT NULL,
  `PERSON_GIVENNAME` varchar(45) NOT NULL,
  `PERSON_ADDRESS` varchar(45) DEFAULT NULL,
  `PERSON_PHONE` varchar(45) DEFAULT NULL,
  `PERSON_CELLPHONE` varchar(45) NOT NULL,
  `PERSON_EMAIL` varchar(45) DEFAULT NULL,
  `PERSON_CITY` varchar(45) DEFAULT NULL,
  `PERSON_STATE` varchar(5) DEFAULT NULL,
  `PERSON_ZIP` varchar(6) DEFAULT NULL,
  `PERSON_BIRTH` varchar(10) DEFAULT NULL,
  `PERSON_SS_NUM` varchar(25) DEFAULT NULL,
  `PERSON_US_CITIZEN` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `management_persons`
--

INSERT INTO `management_persons` VALUES(1, 'SYSTEM', 'SYSTEM', '', '', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `management_persons` VALUES(2, 'ARIZA', 'JONATHAN', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `management_persons` VALUES(3, 'SILVA', 'MIGUEL', 'APENINOS 20', '0', '4441231856', 'mxmigue@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `management_persons` VALUES(4, 'SILVA-DRIVER', 'MIGUEL', 'APENINOS 20', '0', '000000000', 'correo@correo.com', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `management_persons` VALUES(15, 'SURNAME', 'GIVENNAME', '234 ADDRESS', 'HOMEPHONE', 'CELLULLAR', NULL, 'CITY', 'ST', '900909', '2019-09-03', 'SSSNUM', '0');
INSERT INTO `management_persons` VALUES(16, 'MEINENACHNAME', 'MEINENAME', '12312 HAUPSTRASSE', '91400000', '91400220022', NULL, 'YONKERS', 'NY', '987879', '2019-09-03', 'SSSNUMMM', '0');
INSERT INTO `management_persons` VALUES(17, 'FOUR', 'MIGUEL FOUR', '444 FOUR STREET', '9140004444', '9140044444', NULL, 'YONKERS', 'NY', '4444', '2020-01-08', '444444444444', '0');
INSERT INTO `management_persons` VALUES(18, '', '', '', '', '', NULL, '', '', '', '', '', '0');
INSERT INTO `management_persons` VALUES(19, 'TEST', 'TEST', '', '', '', NULL, '', '', '', '', '', '0');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_call_information`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_call_information`;
CREATE TABLE `view_call_information` (
`CALL_ID` int(10) unsigned
,`CALL_OPERATOR` int(10) unsigned
,`CALL_CAR_TYPE` int(10) unsigned
,`CALL_CLIENT` int(10) unsigned
,`CALL_DAY_NUMBER` int(10) unsigned
,`CALL_LINE` varchar(3)
,`CALL_DATE_TIME` datetime
,`CALL_TIMESTAMP` int(10) unsigned
,`CALL_PHONE` varchar(15)
,`CALL_STATUS` enum('NEW','CANCELLED','DISPATCHED','ON TRIP','COMPLETED')
,`CALL_DISPATCH_DATE_TIME` datetime
,`CALL_DISPATCH_TIMESTAMP` int(11)
,`CALL_DISPATCH_TYPE` enum('DRIVER','DISPATCHER','AUTO')
,`CALL_REGISTER_TYPE` enum('CALL','OFFICE','APP')
,`TRIP_ID` int(10) unsigned
,`TRIP_CALL` int(10) unsigned
,`TRIP_VEHICLE` varchar(4)
,`TRIP_PICKUP` int(10) unsigned
,`TRIP_DESTINATION` int(10) unsigned
,`TRIP_STATUS` enum('REQUESTED','ONGOING','COMPLETED','CANCELLED')
,`TRIP_AMOUNT` float
,`TRIP_ETA` varchar(10)
,`TRIP_START_TIME` int(10) unsigned
,`TRIP_END_TIME` int(10) unsigned
,`TRIP_DISTANCE` float
,`CARTYPE_ID` int(10) unsigned
,`CARTYPE_NAME` varchar(45)
,`CARTYPE_PASS_NUMBER` int(11)
,`USER_ID` int(10) unsigned
,`USER_ROLE` int(10) unsigned
,`USER_PERSON` int(10) unsigned
,`USER_USERNAME` varchar(30)
,`USER_PASSWORD` varchar(45)
,`VEHICLE_ID` varchar(4)
,`VEHICLE_TYPE` int(10) unsigned
,`VEHICLE_PLATE` varchar(12)
,`VEHICLE_YEAR` varchar(4)
,`VEHICLE_MODEL` varchar(45)
,`VEHICLE_MAKE` varchar(45)
,`VEHICLE_COLOR` varchar(15)
,`VEHICLE_OWNED` varchar(1)
,`DRIVER_ID` int(10) unsigned
,`DRIVER_CAR` varchar(4)
,`DRIVER_PERSON` int(10) unsigned
,`DRIVER_IMEI` varchar(45)
,`DRIVER_PIN` varchar(4)
,`DRIVER_LICENCE` varchar(45)
,`DRIVER_STATUS` enum('ACTIVE','HOLD','INACTIVE','VACATION','BACKUP','RETIRED')
,`DRIVER_ONLINE` varchar(1)
,`PICKUP_ID` int(10) unsigned
,`PICKUP_MAPS_ID` varchar(45)
,`PICKUP_MAPS_COORD` varchar(200)
,`PICKUP_MAPS_NAME` varchar(250)
,`PICKUP_NUMBER` varchar(15)
,`PICKUP_STREET` varchar(80)
,`PICKUP_CITY` varchar(80)
,`PICKUP_STATE` varchar(80)
,`PICKUP_ZIP` varchar(15)
,`PICKUP_COUNTRY` varchar(65)
,`DEST_ID` int(10) unsigned
,`DEST_MAPS_ID` varchar(45)
,`DEST_MAPS_COORD` varchar(200)
,`DEST_MAPS_NAME` varchar(250)
,`DEST_NUMBER` varchar(15)
,`DEST_STREET` varchar(80)
,`DEST_CITY` varchar(80)
,`DEST_STATE` varchar(80)
,`DEST_ZIP` varchar(15)
,`DEST_COUNTRY` varchar(65)
);

-- --------------------------------------------------------

--
-- Structure for view `view_call_information`
--
DROP TABLE IF EXISTS `view_call_information`;

DROP VIEW IF EXISTS `view_call_information`;
CREATE ALGORITHM=UNDEFINED DEFINER=`miguemx`@`localhost` SQL SECURITY DEFINER VIEW `view_call_information`  AS  select `core_calls`.`CALL_ID` AS `CALL_ID`,`core_calls`.`CALL_OPERATOR` AS `CALL_OPERATOR`,`core_calls`.`CALL_CAR_TYPE` AS `CALL_CAR_TYPE`,`core_calls`.`CALL_CLIENT` AS `CALL_CLIENT`,`core_calls`.`CALL_DAY_NUMBER` AS `CALL_DAY_NUMBER`,`core_calls`.`CALL_LINE` AS `CALL_LINE`,`core_calls`.`CALL_DATE_TIME` AS `CALL_DATE_TIME`,`core_calls`.`CALL_TIMESTAMP` AS `CALL_TIMESTAMP`,`core_calls`.`CALL_PHONE` AS `CALL_PHONE`,`core_calls`.`CALL_STATUS` AS `CALL_STATUS`,`core_calls`.`CALL_DISPATCH_DATE_TIME` AS `CALL_DISPATCH_DATE_TIME`,`core_calls`.`CALL_DISPATCH_TIMESTAMP` AS `CALL_DISPATCH_TIMESTAMP`,`core_calls`.`CALL_DISPATCH_TYPE` AS `CALL_DISPATCH_TYPE`,`core_calls`.`CALL_REGISTER_TYPE` AS `CALL_REGISTER_TYPE`,`core_trips`.`TRIP_ID` AS `TRIP_ID`,`core_trips`.`TRIP_CALL` AS `TRIP_CALL`,`core_trips`.`TRIP_VEHICLE` AS `TRIP_VEHICLE`,`core_trips`.`TRIP_PICKUP` AS `TRIP_PICKUP`,`core_trips`.`TRIP_DESTINATION` AS `TRIP_DESTINATION`,`core_trips`.`TRIP_STATUS` AS `TRIP_STATUS`,`core_trips`.`TRIP_AMOUNT` AS `TRIP_AMOUNT`,`core_trips`.`TRIP_ETA` AS `TRIP_ETA`,`core_trips`.`TRIP_START_TIME` AS `TRIP_START_TIME`,`core_trips`.`TRIP_END_TIME` AS `TRIP_END_TIME`,`core_trips`.`TRIP_DISTANCE` AS `TRIP_DISTANCE`,`core_car_types`.`CARTYPE_ID` AS `CARTYPE_ID`,`core_car_types`.`CARTYPE_NAME` AS `CARTYPE_NAME`,`core_car_types`.`CARTYPE_PASS_NUMBER` AS `CARTYPE_PASS_NUMBER`,`access_users`.`USER_ID` AS `USER_ID`,`access_users`.`USER_ROLE` AS `USER_ROLE`,`access_users`.`USER_PERSON` AS `USER_PERSON`,`access_users`.`USER_USERNAME` AS `USER_USERNAME`,`access_users`.`USER_PASSWORD` AS `USER_PASSWORD`,`core_vehicles`.`VEHICLE_ID` AS `VEHICLE_ID`,`core_vehicles`.`VEHICLE_TYPE` AS `VEHICLE_TYPE`,`core_vehicles`.`VEHICLE_PLATE` AS `VEHICLE_PLATE`,`core_vehicles`.`VEHICLE_YEAR` AS `VEHICLE_YEAR`,`core_vehicles`.`VEHICLE_MODEL` AS `VEHICLE_MODEL`,`core_vehicles`.`VEHICLE_MAKE` AS `VEHICLE_MAKE`,`core_vehicles`.`VEHICLE_COLOR` AS `VEHICLE_COLOR`,`core_vehicles`.`VEHICLE_OWNED` AS `VEHICLE_OWNED`,`management_drivers`.`DRIVER_ID` AS `DRIVER_ID`,`management_drivers`.`DRIVER_CAR` AS `DRIVER_CAR`,`management_drivers`.`DRIVER_PERSON` AS `DRIVER_PERSON`,`management_drivers`.`DRIVER_IMEI` AS `DRIVER_IMEI`,`management_drivers`.`DRIVER_PIN` AS `DRIVER_PIN`,`management_drivers`.`DRIVER_LICENCE` AS `DRIVER_LICENCE`,`management_drivers`.`DRIVER_STATUS` AS `DRIVER_STATUS`,`management_drivers`.`DRIVER_ONLINE` AS `DRIVER_ONLINE`,`pickup`.`PLACE_ID` AS `PICKUP_ID`,`pickup`.`PLACE_MAPS_ID` AS `PICKUP_MAPS_ID`,`pickup`.`PLACE_MAPS_COORD` AS `PICKUP_MAPS_COORD`,`pickup`.`PLACE_MAPS_NAME` AS `PICKUP_MAPS_NAME`,`pickup`.`PLACE_NUMBER` AS `PICKUP_NUMBER`,`pickup`.`PLACE_STREET` AS `PICKUP_STREET`,`pickup`.`PLACE_CITY` AS `PICKUP_CITY`,`pickup`.`PLACE_STATE` AS `PICKUP_STATE`,`pickup`.`PLACE_ZIP` AS `PICKUP_ZIP`,`pickup`.`PLACE_COUNTRY` AS `PICKUP_COUNTRY`,`destination`.`PLACE_ID` AS `DEST_ID`,`destination`.`PLACE_MAPS_ID` AS `DEST_MAPS_ID`,`destination`.`PLACE_MAPS_COORD` AS `DEST_MAPS_COORD`,`destination`.`PLACE_MAPS_NAME` AS `DEST_MAPS_NAME`,`destination`.`PLACE_NUMBER` AS `DEST_NUMBER`,`destination`.`PLACE_STREET` AS `DEST_STREET`,`destination`.`PLACE_CITY` AS `DEST_CITY`,`destination`.`PLACE_STATE` AS `DEST_STATE`,`destination`.`PLACE_ZIP` AS `DEST_ZIP`,`destination`.`PLACE_COUNTRY` AS `DEST_COUNTRY` from (((((((`core_calls` join `core_trips` on((`core_trips`.`TRIP_CALL` = `core_calls`.`CALL_ID`))) join `core_car_types` on((`core_calls`.`CALL_CAR_TYPE` = `core_car_types`.`CARTYPE_ID`))) left join `access_users` on((`core_calls`.`CALL_OPERATOR` = `access_users`.`USER_ID`))) left join `core_vehicles` on((`core_trips`.`TRIP_VEHICLE` = `core_vehicles`.`VEHICLE_ID`))) left join `management_drivers` on((`management_drivers`.`DRIVER_CAR` = `core_vehicles`.`VEHICLE_ID`))) join `core_places` `pickup` on((`pickup`.`PLACE_ID` = `core_trips`.`TRIP_PICKUP`))) join `core_places` `destination` on((`destination`.`PLACE_ID` = `core_trips`.`TRIP_DESTINATION`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_roles`
--
ALTER TABLE `access_roles`
  ADD PRIMARY KEY (`ROLE_ID`);

--
-- Indexes for table `access_users`
--
ALTER TABLE `access_users`
  ADD PRIMARY KEY (`USER_ID`),
  ADD UNIQUE KEY `USER_USERNAME_UNIQUE` (`USER_USERNAME`),
  ADD KEY `USERROLE_idx` (`USER_ROLE`),
  ADD KEY `USERPERSON_idx` (`USER_PERSON`);

--
-- Indexes for table `core_calls`
--
ALTER TABLE `core_calls`
  ADD PRIMARY KEY (`CALL_ID`),
  ADD KEY `CALLUSERREG_idx` (`CALL_OPERATOR`),
  ADD KEY `CALLCARTYPE_idx` (`CALL_CAR_TYPE`),
  ADD KEY `CALLCLIENT_idx` (`CALL_CLIENT`);

--
-- Indexes for table `core_car_types`
--
ALTER TABLE `core_car_types`
  ADD PRIMARY KEY (`CARTYPE_ID`);

--
-- Indexes for table `core_places`
--
ALTER TABLE `core_places`
  ADD PRIMARY KEY (`PLACE_ID`);

--
-- Indexes for table `core_trips`
--
ALTER TABLE `core_trips`
  ADD PRIMARY KEY (`TRIP_ID`),
  ADD KEY `FKTRIPCALL_idx` (`TRIP_CALL`),
  ADD KEY `FKTRIPVEHICLE_idx` (`TRIP_VEHICLE`),
  ADD KEY `FKPICKUPPLACE_idx` (`TRIP_PICKUP`),
  ADD KEY `FKDESTPLACE_idx` (`TRIP_DESTINATION`);

--
-- Indexes for table `core_trip_stops`
--
ALTER TABLE `core_trip_stops`
  ADD PRIMARY KEY (`TRIPSTOP_ID`),
  ADD KEY `FKTRPSTPTRIP_idx` (`TRIPSTOP_TRIP`),
  ADD KEY `FKTRPSTPPLACE_idx` (`TRIPSTOP_PLACE`);

--
-- Indexes for table `core_vehicles`
--
ALTER TABLE `core_vehicles`
  ADD PRIMARY KEY (`VEHICLE_ID`),
  ADD KEY `VEHCARTYPE_idx` (`VEHICLE_TYPE`);

--
-- Indexes for table `management_clients`
--
ALTER TABLE `management_clients`
  ADD PRIMARY KEY (`CLIENT_ID`),
  ADD KEY `CLIENTPERSON_idx` (`CLIENT_PERSON`);

--
-- Indexes for table `management_drivers`
--
ALTER TABLE `management_drivers`
  ADD PRIMARY KEY (`DRIVER_ID`),
  ADD KEY `DRVCAR_idx` (`DRIVER_CAR`),
  ADD KEY `DRVPERSON_idx` (`DRIVER_PERSON`);

--
-- Indexes for table `management_persons`
--
ALTER TABLE `management_persons`
  ADD PRIMARY KEY (`PERSON_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_roles`
--
ALTER TABLE `access_roles`
  MODIFY `ROLE_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `access_users`
--
ALTER TABLE `access_users`
  MODIFY `USER_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `core_calls`
--
ALTER TABLE `core_calls`
  MODIFY `CALL_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `core_car_types`
--
ALTER TABLE `core_car_types`
  MODIFY `CARTYPE_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `core_places`
--
ALTER TABLE `core_places`
  MODIFY `PLACE_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `core_trips`
--
ALTER TABLE `core_trips`
  MODIFY `TRIP_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `core_trip_stops`
--
ALTER TABLE `core_trip_stops`
  MODIFY `TRIPSTOP_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `management_clients`
--
ALTER TABLE `management_clients`
  MODIFY `CLIENT_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `management_drivers`
--
ALTER TABLE `management_drivers`
  MODIFY `DRIVER_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1007;

--
-- AUTO_INCREMENT for table `management_persons`
--
ALTER TABLE `management_persons`
  MODIFY `PERSON_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `access_users`
--
ALTER TABLE `access_users`
  ADD CONSTRAINT `USERPERSON` FOREIGN KEY (`USER_PERSON`) REFERENCES `management_persons` (`PERSON_ID`),
  ADD CONSTRAINT `USERROLE` FOREIGN KEY (`USER_ROLE`) REFERENCES `access_roles` (`ROLE_ID`);

--
-- Constraints for table `core_calls`
--
ALTER TABLE `core_calls`
  ADD CONSTRAINT `CALLCARTYPE` FOREIGN KEY (`CALL_CAR_TYPE`) REFERENCES `core_car_types` (`CARTYPE_ID`),
  ADD CONSTRAINT `CALLCLIENT` FOREIGN KEY (`CALL_CLIENT`) REFERENCES `management_clients` (`CLIENT_ID`),
  ADD CONSTRAINT `CALLUSERREG` FOREIGN KEY (`CALL_OPERATOR`) REFERENCES `access_users` (`USER_ID`);

--
-- Constraints for table `core_trips`
--
ALTER TABLE `core_trips`
  ADD CONSTRAINT `FKDESTPLACE` FOREIGN KEY (`TRIP_DESTINATION`) REFERENCES `core_places` (`PLACE_ID`),
  ADD CONSTRAINT `FKPICKUPPLACE` FOREIGN KEY (`TRIP_PICKUP`) REFERENCES `core_places` (`PLACE_ID`),
  ADD CONSTRAINT `FKTRIPCALL` FOREIGN KEY (`TRIP_CALL`) REFERENCES `core_calls` (`CALL_ID`),
  ADD CONSTRAINT `FKTRIPVEHICLE` FOREIGN KEY (`TRIP_VEHICLE`) REFERENCES `core_vehicles` (`VEHICLE_ID`);

--
-- Constraints for table `core_trip_stops`
--
ALTER TABLE `core_trip_stops`
  ADD CONSTRAINT `FKTRPSTPPLACE` FOREIGN KEY (`TRIPSTOP_PLACE`) REFERENCES `core_places` (`PLACE_ID`),
  ADD CONSTRAINT `FKTRPSTPTRIP` FOREIGN KEY (`TRIPSTOP_TRIP`) REFERENCES `core_trips` (`TRIP_ID`);

--
-- Constraints for table `core_vehicles`
--
ALTER TABLE `core_vehicles`
  ADD CONSTRAINT `VEHCARTYPE` FOREIGN KEY (`VEHICLE_TYPE`) REFERENCES `core_car_types` (`CARTYPE_ID`);

--
-- Constraints for table `management_clients`
--
ALTER TABLE `management_clients`
  ADD CONSTRAINT `CLIENTPERSON` FOREIGN KEY (`CLIENT_PERSON`) REFERENCES `management_persons` (`PERSON_ID`);

--
-- Constraints for table `management_drivers`
--
ALTER TABLE `management_drivers`
  ADD CONSTRAINT `DRVCAR` FOREIGN KEY (`DRIVER_CAR`) REFERENCES `core_vehicles` (`VEHICLE_ID`),
  ADD CONSTRAINT `DRVPERSON` FOREIGN KEY (`DRIVER_PERSON`) REFERENCES `management_persons` (`PERSON_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
