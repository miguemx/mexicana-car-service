<?php

/**
 * Esta clase ha sido nombrada Man por el
 * corto de Management. Aquí se tienen todos los
 * entry points para la administración del sistema
 */
class Man extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('middleware');

        if ( !$this->middleware->verifySession() ) {
            redirect(BASEFRONT.'home','refresh');
        }
        else {
            $module = $this->router->class;
            $method = $this->router->method;
            if ( !$this->middleware->verifyPermission( $this->session->userdata('userid'),$module,$method ) ) {
                redirect(BASEFRONT.'Errors/Denied','refresh');
            }
        }
    }

    public function index() {
        $data['view'] = 'dashboard';
        $this->load->view('manager/layout',$data);
    }

    public function vehicles() {
        $data['view'] = 'vehicles';
        $this->load->view('manager/layout',$data);
    }

    public function drivers() {
        $data['view'] = 'drivers';
        $this->load->view('manager/layout',$data);
    }

    public function settings() {
        $data['view'] = 'settings';
        $this->load->view('manager/layout',$data);
    }

    public function payments() {
        $data['view'] = 'settings';
        $this->load->view('manager/layout',$data);
    }

}