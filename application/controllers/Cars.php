<?php

class Cars extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('middleware');

        if ( !$this->middleware->verifySession() ) {
            redirect(BASEFRONT.'home','refresh');
        }
        else {
            $module = $this->router->class;
            $method = $this->router->method;
            if ( !$this->middleware->verifyPermission( $this->session->userdata('userid'),$module,$method ) ) {
                redirect(BASEFRONT.'Errors/Denied','refresh');
            }
        }
    }

    public function index() {
        $data['view'] = 'vehicles';
        $this->load->view('manager/layout',$data);
    }

    public function nuevo() {
        
    }

}