<?php

class Calls extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->library('session');
        $this->load->library('middleware');

        if ( !$this->middleware->verifySession() ) {
            $res = array("status"=>"error","message"=>"No session");
            echo json_encode($res);
            return 0;
        }
        else {
            $module = $this->router->class;
            $method = $this->router->method;
            if ( !$this->middleware->verifyPermission( $this->session->userdata('userid'),$module,$method ) ) {
                $res = array("status"=>"error","message"=>"No Permission");
                echo json_encode($res);
                return 0;
            }
        }

        $this->load->model('Call');
        $this->load->model('Place');
        $this->load->model('Trip');

        $this->load->library('session');
    }

    public function index() {
        return false;
    }

    public function add() {
        $this->Call->clean();
        $this->Call->line          = $this->input->post('line');
        $this->Call->phone         = $this->input->post('area').$this->input->post('phoneNumber');
        $this->Call->status        = 'NEW';
        $this->Call->registerType  = 'CALL';
        $this->Call->operator      = $this->session->userdata('userid');
        $this->Call->carType       = $this->input->post('carType');
        
        if ( $this->Call->add() ) {
            $placePickup = $this->addPlace( $this->Call->id, $this->input->post('pickup') );
            $placeDestination = $this->addPlace( $this->Call->id, $this->input->post('destination') );
            $amount = $this->input->post('amount');
            $eta = $this->input->post('eta');
            $this->addTrip( $this->Call->id, $placePickup, $placeDestination, $amount, $eta );
            echo json_encode( 
                array( 
                    'status' => 'ok',
                    'data' => $this->Call->searchNewData( $this->Call->id )
                ) 
            );
        }
        else {
            echo json_encode( 
                array(
                    'status' => 'error', 
                    'data' => 'Error al conectar a la base de datos'
                ) 
            );
        }
    }

    public function lista() {
        echo json_encode( $this->Call->getAllData() );
    }

    /**
     * agrega un registro de lugar a la base de datos
     * @param callId el ID de la llamada donde se registra
     * @param placeData la cadena de los datos del lugar, separada por pipes 
     */
    private function addPlace($callId, $placeData) {
        $address = json_decode($placeData,true);
        $data = array(
            'mapsId'    => $address['id'],
            'latitude'  => $address['latitude'],
            'longitude' => $address['longitude'],
            'name'      => $address['name'],
            'number'    => $address['streetNumber'],
            'street'    => $address['street'],
            'city'      => $address['city'],
            'state'     => $address['state'],
            'zip'       => $address['postalCode'],
            'country'   => $address['country']
        );
        $this->Place->findOrAdd($data);
        return $this->Place->id;
    }

    private function addTrip($callId, $pickup, $destination, $amount, $eta) {
        $this->Trip->call        = $callId;
        $this->Trip->pickup      = $pickup;
        $this->Trip->destination = $destination;
        $this->Trip->status      = 'REQUESTED';
        $this->Trip->amount      = $amount;
        $this->Trip->eta         = $eta;
        $this->Trip->distance    = $this->input->post('distance');

        $this->Trip->add();
    }

}
