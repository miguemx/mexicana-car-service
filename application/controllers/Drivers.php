<?php

class Drivers extends CI_Controller {

    /**
     * construye una llamada desde HTTP. carga para todo el controlador la libreria middleware para verificacion
     * de sesion y permisos, activa los headers para habilitar el CORS y poder utilizar la api desde otros origenes
     * y ajusta el tipo de respuesta global.
     * Si el usuario no cuenta con sesion o con permisos, la API responde desde este metodo
     */
    public function __construct() {
        parent::__construct();

        $this->load->library('middleware');
        $this->load->helper('security');

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $this->output->set_content_type('application/json');

        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            $this->output->set_output( json_encode(array('status' => 'error', 'message' => 'Bad request', 'data'=>null)) );
            die();
        }

        if ( !$this->middleware->checkSession() ) {
            $this->output->set_output( json_encode(array('status' => 'error', 'message' => 'You don\'t have permission.', 'data'=>null)) );
            die();
        }
        else {
            $module = $this->router->class;
            $method = $this->router->method;
            if ( !$this->middleware->checkPermission(  ) ) {
                $this->output->set_output( json_encode(array('status' => 'error', 'message' => 'You don\'t have permission.', 'data'=>null)) );
                die();
            }
            $this->load->model('Person');
            $this->load->model('Driver');
            $this->load->model('Car');
        }
    }

    public function index() {
        $res = array('status'=>'error','message'=>'Proccess not started.');
        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $jsonRequest = json_decode($stream_clean,true);
        if ( is_null($jsonRequest) ) {
            $jsonRequest = array();
        }
        
        $data = $this->Driver->getAll( $jsonRequest );
        $res = array('status'=>'ok','message'=>'', 'data'=>$data);

        $this->output->set_output( json_encode($res) );
    }

    public function ver($id) {
        $res = array('status'=>'error','message'=>'Proccess not started.');
        
        $data = $this->Driver->getAll( array('id'=>$id) );
        if ( $data['totalRegistros'] == '1' ) {
            $res = array('status'=>'ok','message'=>'', 'data'=>$data['registros'][0]);
        }
        else {
            $res = array('status'=>'error','message'=>'Driver not found', 'data'=>null);
        }
        

        $this->output->set_output( json_encode($res) );
    }

    public function create() {
        $data['view'] = 'drivers-edit';
        $data['driverId'] = '0';
        $this->load->view('manager/layout',$data);
    }

}