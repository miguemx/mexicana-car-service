<?php

class WebServices extends CI_Controller {

    public function loginService() {
        $this->load->model('Driver');
        $this->load->model('User');

        $res = array('status'=>'error','data'=>'Login proccess not initialized');

        $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $jsonRequest = json_decode($stream_clean,true);
        if ( is_null($jsonRequest) ) {
            $res = array('status'=>'error','data'=>'No parameters given.');
        }
        else {
            $type     = ( array_key_exists('type',     $jsonRequest) )? strtoupper($jsonRequest['type']):     '';
            $username = ( array_key_exists('username', $jsonRequest) )? $jsonRequest['username']: '';
            $password = ( array_key_exists('password', $jsonRequest) )? $jsonRequest['password']: '';
            $driverId = ( array_key_exists('driver',   $jsonRequest) )? $jsonRequest['driver']:   '';
            $pin      = ( array_key_exists('pin',      $jsonRequest) )? $jsonRequest['pin']:      '';
            
            if ( $type == 'DRIVER' ) {
                $login = $this->Driver->identify($driverId,$pin);
            }
            else {
                $this->User->username = $username;
                $this->User->password = sha1( $password );
                $login = $this->User->login();
            }

            if ( $login ) {
                $res = array('status'=>'ok','data'=> array( 'message'=>'Logged in successfuly','user'=> $this->User,'driver'=>$this->Driver ) );
            }
            else {
                $res = array('status'=>'error','data'=>'Username or password doesn\'t match');
            }
        }

        header('Content-Type: application/json');
        echo json_encode($res);
    }

    public function getTripInformation($idCall) {
        $this->load->model('Call');
        $data = $this->Call->searchAllData($idCall);
        echo json_encode($data);
    }

}
