<?php

class Errors extends CI_Controller {

    public function notFound() {
        header("HTTP/1.1 404 Not Found");
        echo '404. Page not found.';
    }

    public function denied() {
        header("HTTP/1.1 401 Unauthorized");
        echo '401. Unauthorized.';
    }

}