<?php

class Drivers_old extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('middleware');

        if ( !$this->middleware->verifySession() ) {
            redirect(BASEFRONT.'home','refresh');
        }
        else {
            $module = $this->router->class;
            $method = $this->router->method;
            if ( !$this->middleware->verifyPermission( $this->session->userdata('userid'),$module,$method ) ) {
                redirect(BASEFRONT.'Errors/Denied','refresh');
            }
            $this->load->model('Person');
            $this->load->model('Driver');
            $this->load->model('Car');
        }
    }

    public function index() {
        $data['view'] = 'drivers-list';
        $this->load->view('manager/layout',$data);
    }

    public function view($id) {
        $data['view'] = 'drivers-edit';
        $this->load->view('manager/layout',$data);
    }

    public function create() {
        $data['view'] = 'drivers-edit';
        $data['driverId'] = '0';
        $this->load->view('manager/layout',$data);
    }

    public function edit($driverId) {
        $data['view'] = 'drivers-edit';
        $data['driverId'] = $driverId;
        $this->load->view('manager/layout',$data);
    }

    public function add() {
        $result = array( 'status' => 'error', 'data' => 'Process not started.' );
        $data = $this->_getDataFromInput();
        $this->Person->importFromArray($data);
        $this->Driver->importFromArray($data);
        $this->Car->importFromArray($data);

        $this->Person->add();
        if ( !is_null($this->Person->id) ) {
            $this->Car->add();
            if ( !is_null($this->Car->id) ) {
                $this->Driver->personId = $this->Person->id;
                $this->Driver->carId = $this->Car->id;
                $this->Driver->add();
                if ( !is_null($this->Driver->id) ) {
                    $result = array( 
                        'status' => 'ok', 
                        'data' => array(
                            'person' => $this->Person,
                            'driver' => $this->Driver,
                            'car' => $this->Car
                        ) 
                    );
                }
                else {
                    $result = array( 'status' => 'error', 'data' => 'Cannot register driver.' );
                }
            }
            else {
                $result = array( 'status' => 'error', 'data' => 'Cannot register car.' );
            }
        }
        else {
            $result = array( 'status' => 'error', 'data' => 'Cannot register person.' );
        }

        echo json_encode($result);
    }

    public function lista() {
        $result = array( 'status' => 'ok', 'data' => $this->Driver->lista() );
        echo json_encode($result);
    }

    public function update() {
        $result = array( 'status' => 'error', 'data' => 'Process not started.' );
        $data = $this->_getDataFromInput();
        $this->Person->importFromArray($data);
        $this->Driver->importFromArray($data);
        $this->Car->importFromArray($data);

        $this->Person->add();
        if ( !is_null($this->Person->id) ) {
            $this->Car->add();
            if ( !is_null($this->Car->id) ) {
                $this->Driver->personId = $this->Person->id;
                $this->Driver->carId = $this->Car->id;
                $this->Driver->add();
                if ( !is_null($this->Driver->id) ) {
                    $result = array( 
                        'status' => 'ok', 
                        'data' => array(
                            'person' => $this->Person,
                            'driver' => $this->Driver,
                            'car' => $this->Car
                        ) 
                    );
                }
                else {
                    $result = array( 'status' => 'error', 'data' => 'Cannot register driver.' );
                }
            }
            else {
                $result = array( 'status' => 'error', 'data' => 'Cannot register car.' );
            }
        }
        else {
            $result = array( 'status' => 'error', 'data' => 'Cannot register person.' );
        }

        echo json_encode($result);
    }


    private function _getDataFromInput() {
        $input_stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
        $data = json_decode ( $input_stream_clean, true );
        foreach( $data as $key=>$value ) {
            if ( gettype($value) == 'string' ) {
                $data[$key] = strtoupper($value);
            }
        }
        return $data;
    }

}