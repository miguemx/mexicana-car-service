<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('Middleware');
        $this->load->helper('url');
    }

	public function index()
	{
        $message = $this->input->get('msg');
        $data['message'] = ( is_null($message) )? "": $message;
		$this->load->view('login',$data);
	}

	public function login() {
        $dataPost = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'type' => 'SYS',
        );
        $dataRawPost = json_encode($dataPost);

        $ch = curl_init(); // Crear un nuevo recurso cURL

        curl_setopt($ch, CURLOPT_URL, BASEFRONT.'WebServices/LoginService');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataRawPost);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($dataRawPost))
        );

        $res = curl_exec($ch);
        $loginResult = json_decode($res,true);
        curl_close($ch);

        if ( $loginResult['status'] == 'ok' ) {
            $this->middleware->setSession( $loginResult['data']['user']['id'], $loginResult['data']['user']['roleId'] );
            redirect(BASEFRONT.$loginResult['data']['user']['role']['url'],'refresh');
        }
        else { var_dump($res);
            // redirect(BASEFRONT.'Home?msg='.$loginResult['data'],'refresh');
        }
	}

	public function logout() {
        $this->session->unset_userdata('userid');
        $this->session->unset_userdata('roleid');
        redirect(BASEFRONT,'refresh');
	}
}
