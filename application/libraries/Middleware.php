<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Middleware {

    private $_ci = null;
    public $message = '';

    private $_User;

    public function __construct() {
        $this->_ci = &get_instance();
    }

    /**
     * creates a session cookie to identify all user
     * @param userId the user's id
     * @param roleId the user's role Id
     */
    public function setSession($userId, $roleId) {
        $this->_ci->session->set_userdata('userid',$userId);
        $this->_ci->session->set_userdata('roleid',$roleId);
    }

    public function destroySession() {
        $this->_ci->session->unset_userdata('userid');
        $this->_ci->session->unset_userdata('roleid');
    }

    public function verifySession() {
        $res = false;
        $userId = $this->_ci->session->userdata('userid');
        $roleId = $this->_ci->session->userdata('roleid');
        if( !is_null($userId) && !is_null($roleId) ) {
            $res = true;
        }
        else {
            $this->message = 'No session. Please login.';
        }
        return $res;
    }

    public function verifyPermission($userID, $module, $method) {
        $permission = false;
        $this->_ci->load->model('User');

        $this->_User = $this->_ci->User;
        $this->_User->find($userID);

        if ( !is_null($this->_User->id) ) {
            $module = strtolower($module);
            $adminModules = array('man','dispatcher','calls','cars','drivers','payments');
            $operModules = array('dispatcher','calls');

            switch ( $this->_User->roleId ) {
                case '1': $permission = in_array( $module, $adminModules ); break;
                case '2': $permission = in_array( $module, $operModules ); break;
                default: break;
            }
        }
        return $permission;
    }

    public function checkSession() {
        return true;
    }

    public function checkPermission() {
        return true;
    }

    public function updateToken() {
        return true;
    }

}