<?php

class Role extends CI_Model { 

    public $id;
    public $name;
    public $url;

    private $_table = 'access_roles';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * imports values to database, from object's properties
     */
    public function import() {
        return(
            array(
                'role_id'   => $this->id,
                'role_name' => $this->name,
                'role_url'  => $this->url,
            )
        );
    }

    /**
     * passes data to object's properties, form a query to database
     * @param row object with database information
     */
    public function export($row) {
        $this->id   = $row->role_id;
        $this->name = $row->role_name;
        $this->url  = $row->role_url;
    }

    /**
     * resets object's properties
     */
    public function clean() {
        $this->id   = null;
        $this->name = null;
        $this->url  = null;
    }

    public function find($id) {
        $this->clean();
        $this->db->where("role_id",$id);
        $query = $this->db->get( $this->_table );
        $rows = $query->result();
        if ( count($rows) >= 1 ) {
            $this->export( $rows[0] );
        }
    }

}