<?php

class Trip extends CI_Model {

    public $id;
    public $call;
    public $vehicle;
    public $pickup;
    public $destination;
    public $status;
    public $amount;
    public $eta;
    public $startTime;
    public $endTime;
    public $distance;


    private $table = 'CORE_TRIPS';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * toma los valores de cada propiedad para ajustarlas al arreglo que
     * deberá ser pasado a la base de datos
     * @return arreglo un arreglo con las llaves que son nombres de los campos y sus respectivos valores
     */
    public function import() {
        return array(
            'trip_id'          => $this->id,
            'trip_call'        => $this->call,
            'trip_vehicle'     => $this->vehicle,
            'trip_pickup'      => $this->pickup,
            'trip_destination' => $this->destination,
            'trip_status'      => $this->status,
            'trip_amount'      => $this->amount,
            'trip_eta'         => $this->eta,
            'trip_start_time'  => $this->startTime,
            'trip_end_time'    => $this->endTime,
            'trip_distance'    => $this->distance
        );
    }

    /**
     * pone las propiedades de la clase con los valores obtenidos de una consulta a la base de datos
     * @param registro objeto que se obtiene de una consulta de base de datos
     */
    public function export($registro) {
        $this->id          = $registro->trip_id;
        $this->call        = $registro->trip_call;
        $this->vehicle     = $registro->trip_vehicle;
        $this->pickup      = $registro->trip_pickup;
        $this->destination = $registro->trip_destination;
        $this->status      = $registro->trip_status;
        $this->amount      = $registro->trip_amount;
        $this->eta         = $registro->trip_eta;
        $this->startTime   = $registro->trip_start_time;
        $this->endTime     = $registro->trip_end_time;
        $this->distance    = $registro->trip_distance;
    }

    /**
     * agrega un nuevo registro de llamada a la base de datos
     * @return result true si se realiza la insercion
     */
    public function add() {
        $result = false;
        try {
            $data = $this->import();
            $this->db->set($data);
            $this->db->insert( $this->table );
            $this->id = $this->db->insert_id();
            $result = true;
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
        }
        return $result;
    }

    /**
     * actualiza un nuevo registro de llamada a la base de datos
     * @return result true si se ejecuta correctamente la actualización
     */
    public function update() {
        $result = false;
        if ( !is_null( $this->id ) ) {
            try {
                $data = $this->import();
                $this->db->where('trip_id',$this->id);
                $this->db->update( $this->table ,$data);
                $result = true;
            }
            catch ( Exception $ex ) {
                echo $ex->getMessage();
            }
        }
        return $result;
    }

    /**
     * guarda un registro, ya sea nuevo o actualizado
     * @return result true cuando se guarda el registro
     */
    public function save() {
        if ( is_null( $this->id ) ) {
            $result = $this->add();
        }
        else {
            $result = $this->update();
        }
        return $result;
    }

    /**
     * limpia las propiedades del objeto, dejando todas en null
     */
    public function clean() {
        $this->id          = null;
        $this->call        = null;
        $this->vehicle     = null;
        $this->pickup      = null;
        $this->destination = null;
        $this->status      = null;
        $this->amount      = null;
        $this->eta         = null;
        $this->startTime   = null;
        $this->endTime     = null;
        $this->distance    = null;
    }

    /**
     * encuentra un viaje con base en su id
     * si no es encontrado, el objeto quedara en nulos
     * @param id el ID a buscar
     */
    public function findById($id) {
        $this->clean();
        $this->db->where('trip_id',$id);
        $query = $this->db->get( $this->table, 1 );
        $data = $query->result();
        foreach( $data as $row ) {
            $this->export($row);
        }
    }

    /**
     * encuentra un viaje con base en la llamada que lo registro
     * si no es encontrado el objeto quedara en nulos
     * @param call el id de llamada
     */
    public function findByCall($call) {
        $this->clean();
        $this->db->where('trip_call',$call);
        $query = $this->db->get( $this->table, 1 );
        $data = $query->result();
        foreach( $data as $row ) {
            $this->export($row);
        }
    }

}