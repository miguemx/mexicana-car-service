<?php

class Car extends CI_Model {

    public $id;
    public $typeId;
    public $plate;
    public $year;
    public $model;
    public $make;
    public $color;
    public $owned;
    public $regExpDate;
    public $insPolicy;
    public $insExpDate;
    public $insCompany;
    public $vI;
    public $tlcDiamond;
    public $diamondExpDate;
    public $inspectionDate;
    public $registredName;

    private $_table = 'core_vehicles';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * fills object's properties with a record from database
     */
    public function export($record) {
        $this->id             = $record->vehicle_id;
        $this->typeId         = $record->vehicle_type;
        $this->plate          = $record->vehicle_plate;
        $this->year           = $record->vehicle_year;
        $this->model          = $record->vehicle_model;
        $this->make           = $record->vehicle_make;
        $this->color          = $record->vehicle_color;
        $this->owned          = $record->vehicle_owned;
        $this->regExpDate     = $record->vehicle_reg_exp_date;
        $this->insPolicy      = $record->vehicle_ins_policy;
        $this->insExpDate     = $record->vehicle_ins_exp_date;
        $this->insCompany     = $record->vehicle_ins_company;
        $this->vI             = $record->vehicle_v_i;
        $this->tlcDiamond     = $record->vehicle_tlc_diamond;
        $this->diamondExpDate = $record->vehicle_diamond_exp_date;
        $this->inspectionDate = $record->vehicle_inspection_date;
        $this->registredName  = $record->vehicle_registred_name; 
    }

    /**
     * imports a database record from object's properties
     */
    public function import() {
        return array(
            'vehicle_id' => $this->id,
            'vehicle_type' => $this->typeId,
            'vehicle_plate' => $this->plate,
            'vehicle_year' => $this->year,
            'vehicle_model' => $this->model,
            'vehicle_make' => $this->make,
            'vehicle_color' => $this->color,
            'vehicle_owned' => $this->owned,
            'vehicle_reg_exp_date' => $this->regExpDate,
            'vehicle_ins_policy' => $this->insPolicy,
            'vehicle_ins_exp_date' => $this->insExpDate,
            'vehicle_ins_company' => $this->insCompany,
            'vehicle_v_i' => $this->vI,
            'vehicle_tlc_diamond' => $this->tlcDiamond,
            'vehicle_diamond_exp_date' => $this->diamondExpDate,
            'vehicle_inspection_date' => $this->inspectionDate,
            'vehicle_registred_name' => $this->registredName,
        );
    }

    public function importFromArray($array) {
        $this->id             = (array_key_exists('carId',$array))? $array['carId']: null;
        $this->typeId         = (array_key_exists('carTypeId',$array))? $array['carTypeId']: null;
        $this->plate          = (array_key_exists('plate',$array))? $array['plate']: null;
        $this->year           = (array_key_exists('year',$array))? $array['year']: null;
        $this->model          = (array_key_exists('model',$array))? $array['model']: null;
        $this->make           = (array_key_exists('make',$array))? $array['make']: null;
        $this->color          = (array_key_exists('carColor',$array))? $array['carColor']: null;
        $this->owned          = (array_key_exists('owned',$array))? $array['owned']: null;
        $this->regExpDate     = (array_key_exists('regExpDate',$array))? $array['regExpDate']: null;
        $this->insPolicy      = (array_key_exists('insPolicy',$array))? $array['insPolicy']: null;
        $this->insExpDate     = (array_key_exists('insExpDate',$array))? $array['insExpDate']: null;
        $this->insCompany     = (array_key_exists('insCompany',$array))? $array['insCompany']: null;
        $this->vI             = (array_key_exists('vI',$array))? $array['vI']: null;
        $this->tlcDiamond     = (array_key_exists('tlcDiamond',$array))? $array['tlcDiamond']: null;
        $this->diamondExpDate = (array_key_exists('diamondExpDate',$array))? $array['diamondExpDate']: null;
        $this->inspectionDate = (array_key_exists('inspectionDate',$array))? $array['inspectionDate']: null;
        $this->registredName  = (array_key_exists('registredName',$array))? $array['registredName']: null;
    }

    /**
     * resets object's properties
     */
    public function clean() {
        $this->id             = null;
        $this->typeId         = null;
        $this->plate          = null;
        $this->year           = null;
        $this->model          = null;
        $this->make           = null;
        $this->color          = null;
        $this->owned          = null;
        $this->regExpDate     = null;
        $this->insPolicy      = null;
        $this->insExpDate     = null;
        $this->insCompany     = null;
        $this->vI             = null;
        $this->tlcDiamond     = null;
        $this->diamondExpDate = null;
        $this->inspectionDate = null;
        $this->registredName  = null;
    }

    public function add() {
        if ( is_null($this->id) || $this->id == '0' || $this->id == 0 ) {
            $this->id = $this->getNextCar();
            $this->db->set( $this->import() );
            $this->db->insert( $this->_table );
        }
    }

    /**
     * finds a car using their ID
     * @param id id to search
     */
    public function find($id) {
        $this->clean();
        $this->db->where('vehicle_id',$id);
        $query = $this->db->get( $this->_table );
        $rows = $query->result();
        if ( count($rows) >= 1 ) {
            $this->export( $rows[0] );
            return true;
        }
        return false;
    }

    public function getNextCar() {
        $nextCar = '0';
        $this->db->select_max('vehicle_id');
        $query = $this->db->get( $this->_table );
        $rows = $query->result();
        if ( count($rows) == 1 ) {
            $currentLast = intval( $rows[0]->VEHICLE_ID ) + 1;
            $nextCar = $currentLast.'';
        }
        return $nextCar;
    }

    /**
     * obtiene todos los registros de las personas registradas en la base de datos
     * @param filtros un arreglo con las llaves para componer los filtros
     * @param regXPagina numero de registros que devolvera
     * @param pagina numero de pagina a regresar
     */
    public function getAll( $filtros=array(), $regXPagina=100, $pagina=1 ) {
        $registros = $this->createPage( $filtros, $regXPagina, $pagina );
        $registros['pagina'] = $pagina;
        $registros['registros'] = array();
        $this->db->select('*');
		$this->from();
        $this->createFilter($filtros);
        $this->db->order_by('driver_car ASC');
        $this->db->limit( $regXPagina, $registros['inicio'] );

        $query = $this->db->get();
        $rows = $query->result();
        
        foreach ( $rows as $row ) {
            $registros['registros'][] = array(
                'driverId' => $row->driver_id,
                'car' => $row->driver_car,
                'imei' => $row->driver_imei,
                'pin' => $row->driver_pin,
                'status' => $row->driver_status,
                'online' => $row->driver_online,
                'owner' => $row->driver_owner,
                'drvdn' => $row->driver_drvdn,
                'licence' => $row->driver_licence,
                'licenceType' => $row->driver_licence_type,
                'licenceExp' => $row->driver_licence_exp,
                'tlcLic' => $row->driver_tlc_lic,
                'tlcExpDate' => $row->driver_tlc_expdate,
                'beginData' => $row->driver_begin_date,
                'tlcUrineExp' => $row->driver_tlc_urine_exp,

                'personId' => $row->person_id,
                'surname' => $row->person_surname,
                'givenname' => $row->person_givenname,
                'address' => $row->person_address,
                'phone' => $row->person_phone,
                'cellphone' => $row->person_cellphone,
                'email' => $row->person_email,
                'city' => $row->person_city,
                'state' => $row->person_state,
                'zip' => $row->person_zip,
                'birth' => $row->person_birth,
                'ssNumber' => $row->person_ss_num,
                'usCitizen' => $row->person_us_citizen
            );
        }

		return $registros;
    }

    /**
     * crea un filtro para la consulta, y selecciona la cantidad de registros y su respectivo indice de inicio y fin
     */
    private function createPage( $filtros, $regXPagina, $pagina) {
        $this->db->select('COUNT(driver_id) as REG');
		$this->from();
        $this->createFilter($filtros);
        $query = $this->db->get();
        $rows = $query->result();
        $numRows = intval($rows[0]->REG);

        $paginas = ceil( $numRows/$regXPagina );
        $inicio = ($pagina - 1 ) * $regXPagina;

        if ( $numRows <= $inicio ) {
            $inicio = 0;
        }

        return (
            array(
                'totalRegistros' => $numRows,
                'inicio' => $inicio,
                'regXPagina' => $regXPagina,
                'paginas' => $paginas
            )
            );
    }

    /**
     * crea las clausulas where para la consulta de obtencion de las preguntas
     * @param filtros un array con las llaves como condiciones, y sus valores como la busqueda en la BD
     * @author miguemx
     */
    private function createFilter( $filtros ) {
        if ( array_key_exists('id', $filtros) ) { // filtrar por driver
            if ( $filtros['id'] ) {
                $this->db->where( 'driver_id', $filtros['id'] );
            }
        }

        if ( array_key_exists('car', $filtros) ) { // filtrar por driver
            if ( $filtros['car'] ) {
                $this->db->where( 'driver_car', $filtros['car'] );
            }
        }
    }

    private function from() {
        $this->db->from( $this->_view );
    }

}
