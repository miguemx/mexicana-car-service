<?php

class Driver extends CI_Model {

    public $id;
    public $carId;
    public $personId;
    public $imei;
    public $pin;
    public $status;
    public $online;
    public $owner;
    public $drvdn;
    public $licence;
    public $licenceType;
    public $licenceExpDate;
    public $tlcLic;
    public $tlcExpDate;
    public $beginDate;
    public $tlcUrineExp;

    public $car;

    private $_table = 'management_drivers';
    private $_view = 'view_drivers';

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Car');
    }

    /**
     * fills object's properties with a record from database
     */
    public function export($record) {
        $this->id             = $record->driver_id;
        $this->carId          = $record->driver_car;
        $this->personId       = $record->driver_person;
        $this->imei           = $record->driver_imei;
        $this->pin            = $record->driver_pin;
        $this->status         = $record->driver_status;
        $this->online         = $record->driver_online;
        $this->owner          = $record->driver_owner;
        $this->drvdn          = $record->driver_drvdn;
        $this->licence        = $record->driver_licence;
        $this->licenceType    = $record->driver_licence_type;
        $this->licenceExpDate = $record->driver_licence_exp;
        $this->tlcLic         = $record->driver_tlc_lic;
        $this->tlcExpDate     = $record->driver_tlc_expdate;
        $this->beginDate      = $record->driver_begin_date;
        $this->tlcUrineExp    = $record->driver_tlc_urine_exp;
    }

    /**
     * imports a database record from object's properties
     */
    public function import() {
        return array(
            'driver_id' => $this->id,
            'driver_car' => $this->carId,
            'driver_person' => $this->personId,
            'driver_imei' => $this->imei,
            'driver_pin' => $this->pin,
            'driver_status' => $this->status,
            'driver_online' => $this->online,
            'driver_owner' => $this->owner,
            'driver_drvdn' => $this->drvdn,
            'driver_licence' => $this->licence,
            'driver_licence_type' => $this->licenceType,
            'driver_licence_exp' => $this->licenceExpDate,
            'driver_tlc_lic' => $this->tlcLic,
            'driver_tlc_expdate' => $this->tlcExpDate,
            'driver_begin_date' => $this->beginDate,
            'driver_tlc_urine_exp' => $this->tlcUrineExp,
        );
    }

    /**
     * imports to prperties from an array
     */
    public function importFromArray($array) {
        $this->id             = (array_key_exists('driverId',$array))? $array['driverId']: null;
        $this->carId          = (array_key_exists('carId',$array))? $array['carId']: null;
        $this->personId       = (array_key_exists('personId',$array))? $array['personId']: null;
        $this->imei           = (array_key_exists('drvn',$array))? $array['drvn']: null;
        $this->pin            = (array_key_exists('drvn',$array))? $array['drvn']: null;
        $this->status         = (array_key_exists('driverStatus',$array))? $array['driverStatus']: null;
        $this->online         = '1';
        $this->owner          = '0';
        $this->drvdn          = (array_key_exists('drvn',$array))? $array['drvn']: null;
        $this->licence        = (array_key_exists('licence',$array))? $array['licence']: null;
        $this->licenceType    = (array_key_exists('licenceType',$array))? $array['licenceType']: null;
        $this->licenceExpDate = (array_key_exists('licenceExpDate',$array))? $array['licenceExpDate']: null;
        $this->tlcLic         = (array_key_exists('tlcLic',$array))? $array['tlcLic']: null;
        $this->tlcExpDate     = (array_key_exists('tlcExpDate',$array))? $array['tlcExpDate']: null;
        $this->beginDate      = (array_key_exists('beginDate',$array))? $array['beginDate']: null;
        $this->tlcUrineExp    = (array_key_exists('tlcUrineExp',$array))? $array['tlcUrineExp']: null;
    }

    /**
     * resets object's properties
     */
    public function clean() {
        $this->id             = null;
        $this->carId          = null;
        $this->personId       = null;
        $this->imei           = null;
        $this->pin            = null;
        $this->status         = null;
        $this->online         = null;
        $this->owner          = null;
        $this->drvdn          = null;
        $this->licence        = null;
        $this->licenceType    = null;
        $this->licenceExpDate = null;
        $this->tlcLic         = null;
        $this->tlcExpDate     = null;
        $this->beginDate      = null;
        $this->tlcUrineExp    = null;
    }

    /**
     * adds a driver record
     */
    public function add() {
        if ( is_null($this->id) || $this->id == '0' || $this->id == 0 ) {
            $this->db->set( $this->import() );
            $this->db->insert( $this->_table );
            $this->id = $this->db->insert_id();
        }
    }

    /**
     * gets car object with information
     */
    public function getCar() {
        if ( !is_null($this->carId) ) {
            if ( $this->Car->find( $this->carId ) ) {
                $this->car = $this->Car;
            }
        }
    }

    /**
     * identifica un conductor
     */
    public function identify($driverId,$pin) {
        $this->clean();
        $this->db->where( 'driver_id', $driverId );
        $this->db->where( 'driver_pin', $pin );
        $query = $this->db->get( $this->_table );
        $rows = $query->result();
        if ( count($rows) == 1 ) {
            $this->export( $rows[0] );
            return true;
        }
        return false;
    }

    public function lista() {
        $data = array('rows'=>0,'results'=>array());
        $this->clean();
        $this->db->join('management_persons', 'driver_person = person_id');
        $query = $this->db->get('management_drivers');
        $rows = $query->result();
        $data['rows'] = count($rows);
        foreach( $rows as $row ) {
            $data['results'][] = array(
                'driverId' => $row->driver_id,
                'car' => $row->driver_car,
                'imei' => $row->driver_imei,
                'pin' => $row->driver_pin,
                'status' => $row->driver_status,
                'online' => $row->driver_online,
                'owner' => $row->driver_owner,
                'drvdn' => $row->driver_drvdn,
                'licence' => $row->driver_licence,
                'licenceType' => $row->driver_licence_type,
                'licenceExp' => $row->driver_licence_exp,
                'tlcLic' => $row->driver_tlc_lic,
                'tlcExpDate' => $row->driver_tlc_expdate,
                'beginData' => $row->driver_begin_date,
                'tlcUrineExp' => $row->driver_tlc_urine_exp,

                'personId' => $row->person_id,
                'surname' => $row->person_surname,
                'givenname' => $row->person_givenname,
                'address' => $row->person_address,
                'phone' => $row->person_phone,
                'cellphone' => $row->person_cellphone,
                'email' => $row->person_email,
                'city' => $row->person_city,
                'state' => $row->person_state,
                'zip' => $row->person_zip,
                'birth' => $row->person_birth,
                'ssNumber' => $row->person_ss_num,
                'usCitizen' => $row->person_us_citizen
            );
        }
        return $data;
    }

    /**
     * obtiene todos los registros de las personas registradas en la base de datos
     * @param filtros un arreglo con las llaves para componer los filtros
     * @param regXPagina numero de registros que devolvera
     * @param pagina numero de pagina a regresar
     */
    public function getAll( $filtros=array(), $regXPagina=100, $pagina=1 ) {
        $registros = $this->createPage( $filtros, $regXPagina, $pagina );
        $registros['pagina'] = $pagina;
        $registros['registros'] = array();
        $this->db->select('*');
		$this->from();
        $this->createFilter($filtros);
        $this->db->order_by('driver_car ASC');
        $this->db->limit( $regXPagina, $registros['inicio'] );

        $query = $this->db->get();
        $rows = $query->result();
        
        foreach ( $rows as $row ) {
            $registros['registros'][] = array(
                'id' => $row->driver_id,
                'car' => $row->driver_car,
                'imei' => $row->driver_imei,
                'pin' => $row->driver_pin,
                'status' => $row->driver_status,
                'online' => $row->driver_online,
                'owner' => $row->driver_owner,
                'drvdn' => $row->driver_drvdn,
                'licence' => $row->driver_licence,
                'licenceType' => $row->driver_licence_type,
                'licenceExp' => $row->driver_licence_exp,
                'tlcLic' => $row->driver_tlc_lic,
                'tlcExpDate' => $row->driver_tlc_expdate,
                'beginDate' => $row->driver_begin_date,
                'tlcUrineExp' => $row->driver_tlc_urine_exp,

                'vehicleId' => $row->vehicle_id,
                'vehicleType' => $row->vehicle_type,
                'plate' => $row->vehicle_plate,
                'year' => $row->vehicle_year,
                'model' => $row->vehicle_model,
                'make' => $row->vehicle_make,
                'color' => $row->vehicle_color,
                'owned' => $row->vehicle_owned,
                'regExpDate' => $row->vehicle_reg_exp_date,
                'insurancePolicy' => $row->vehicle_ins_policy,
                'insuranceExpDate' => $row->vehicle_ins_exp_date,
                'insuranceCompany' => $row->vehicle_ins_company,
                'vehicleVi' => $row->vehicle_v_i,
                'tlcDiamond' => $row->vehicle_tlc_diamond,
                'diamondExpDate' => $row->vehicle_diamond_exp_date,
                'inspectionDate' => $row->vehicle_inspection_date,
                'registredName' => $row->vehicle_registred_name,


                'personId' => $row->person_id,
                'surname' => $row->person_surname,
                'givenname' => $row->person_givenname,
                'address' => $row->person_address,
                'phone' => $row->person_phone,
                'cellphone' => $row->person_cellphone,
                'email' => $row->person_email,
                'city' => $row->person_city,
                'state' => $row->person_state,
                'zip' => $row->person_zip,
                'birth' => $row->person_birth,
                'ssNumber' => $row->person_ss_num,
                'usCitizen' => $row->person_us_citizen
            );
        }

		return $registros;
    }

    /**
     * crea un filtro para la consulta, y selecciona la cantidad de registros y su respectivo indice de inicio y fin
     */
    private function createPage( $filtros, $regXPagina, $pagina) {
        $this->db->select('COUNT(driver_id) as REG');
		$this->from();
        $this->createFilter($filtros);
        $query = $this->db->get();
        $rows = $query->result();
        $numRows = intval($rows[0]->REG);

        $paginas = ceil( $numRows/$regXPagina );
        $inicio = ($pagina - 1 ) * $regXPagina;

        if ( $numRows <= $inicio ) {
            $inicio = 0;
        }

        return (
            array(
                'totalRegistros' => $numRows,
                'inicio' => $inicio,
                'regXPagina' => $regXPagina,
                'paginas' => $paginas
            )
            );
    }

    /**
     * crea las clausulas where para la consulta de obtencion de las preguntas
     * @param filtros un array con las llaves como condiciones, y sus valores como la busqueda en la BD
     * @author miguemx
     */
    private function createFilter( $filtros ) {
        if ( array_key_exists('id', $filtros) ) { // filtrar por driver
            if ( $filtros['id'] ) {
                $this->db->where( 'driver_id', $filtros['id'] );
            }
        }

        if ( array_key_exists('car', $filtros) ) { // filtrar por driver
            if ( $filtros['car'] ) {
                $this->db->where( 'driver_car', $filtros['car'] );
            }
        }
    }

    private function from() {
        $this->db->from( $this->_view );
    }
    
}
