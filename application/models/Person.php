<?php

class Person extends CI_Model {

    public $id;
    public $surname;
    public $givenname;
    public $address;
    public $phone;
    public $cellphone;
    public $email;
    public $city;
    public $state;
    public $zip;
    public $birth;
    public $ssNumber;
    public $usCitizen;

    private $_table = 'management_persons';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * fills object's properties with a record from database
     */
    public function export($record) {
        $this->id        = $record->person_id;
        $this->surname   = $record->person_surname;
        $this->givenname = $record->person_gievnname;
        $this->address   = $record->parson_address;
        $this->phone     = $record->person_phone;
        $this->cellphone = $record->person_cellphone;
        $this->email     = $record->person_email;
        $this->city      = $record->person_city;
        $this->state     = $record->person_state;
        $this->zip       = $record->person_zip;
        $this->birth     = $record->person_birth;
        $this->ssNumber  = $record->person_ss_num;
        $this->usCitizen = $record->person_us_citizen;
    }

    /**
     * imports a database record from object's properties
     */
    public function import() {
        return array(
            'person_id' => $this->id,
            'person_surname' => $this->surname,
            'person_gievnname' => $this->givenname,
            'parson_address' => $this->address,
            'person_phone' => $this->phone,
            'person_cellphone' => $this->cellphone,
            'person_email' => $this->email,
            'person_city' => $this->city,
            'person_state' => $this->state,
            'person_zip' => $this->zip,
            'person_birth' => $this->birth,
            'person_ss_num' => $this->ssNumber,
            'person_us_citizen' => $this->usCitizen,
        );
    }

    /**
     * imports into object's properties from an array
     */
    public function importFromArray($array) {
        $this->id        = (array_key_exists('personId',$array))? $array['personId']: null;
        $this->surname   = (array_key_exists('surname',$array))? $array['surname']: null;
        $this->givenname = (array_key_exists('givenname',$array))? $array['givenname']: null;
        $this->address   = (array_key_exists('address',$array))? $array['address']: null;
        $this->phone     = (array_key_exists('phone',$array))? $array['phone']: null;
        $this->cellphone = (array_key_exists('cellphone',$array))? $array['cellphone']: null;
        $this->email     = (array_key_exists('email',$array))? $array['email']: null;
        $this->city      = (array_key_exists('city',$array))? $array['city']: null;
        $this->state     = (array_key_exists('state',$array))? $array['state']: null;
        $this->zip       = (array_key_exists('zip',$array))? $array['zip']: null;
        $this->birth     = (array_key_exists('birth',$array))? $array['birth']: null;
        $this->ssNumber  = (array_key_exists('ssNumber',$array))? $array['ssNumber']: null;
        $this->usCitizen = '0';
    }

    /**
     * resets object's properties
     */
    public function clean() {
        $this->id        = null;
        $this->surname   = null;
        $this->givenname = null;
        $this->address   = null;
        $this->phone     = null;
        $this->cellphone = null;
        $this->email     = null;
        $this->city      = null;
        $this->state     = null;
        $this->zip       = null;
        $this->birth     = null;
        $this->ssNumber  = null;
        $this->usCitizen = null;
    }

    public function add() {
        if ( is_null($this->id) || $this->id == '0' || $this->id == 0 ) {
            $this->db->set( $this->import() );
            $this->db->insert( $this->_table );
            $this->id = $this->db->insert_id();
        }
    }

}