<?php

class User extends CI_Model {

    public $id;
    public $roleId;
    public $username;
    public $password;
    public $surname;

    public $role = null;

    private $_table = 'access_users';

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Role');
    }

    /**
     * imports values to database, from object's properties
     */
    public function import() {
        return(
            array(
                'user_id'        => $this->id,
                'user_role'      => $this->roleId,
                'user_username'  => $this->username,
                'user_password'  => $this->$this->password,
            )
        );
    }

    /**
     * passes data to object's properties, form a query to database
     * @param row object with database information
     */
    public function export($row) {
        $this->id        = $row->user_id;
        $this->roleId    = $row->user_role;
        $this->username  = $row->user_username;
        $this->password  = $row->user_password;
        
    }

    /**
     * resets object's properties
     */
    public function clean() {
        $this->id        = null;
        $this->roleId    = null;
        $this->username  = null;
        $this->password  = null;
    }

    /**
     * 
     */
    public function login() {
        $res = false;
        $this->db->where( 'user_username', $this->username );
        $this->db->where( 'user_password', $this->password );
        $query = $this->db->get( $this->_table );
        $rows = $query->result();
        if ( count($rows) == 1 ) {
            $this->export( $rows[0] );
            $this->Role->find($this->roleId);
            $this->role = $this->Role;
            $res = true;
        }
        return $res;
    }

    public function find($id) {
        $this->clean();
        $this->db->where( 'user_id', $id );
        $query = $this->db->get( $this->_table );
        $rows = $query->result();
        if ( count($rows) == 1 ) {
            $this->export( $rows[0] );
            $this->Role->find($this->roleId);
            $this->role = $this->Role;
            return true;
        }
        return false;
    }

}