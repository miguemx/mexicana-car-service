<?php

class Place extends CI_Model {

    public $id;
    public $mapsId;
    public $mapsCord;
    public $name;
    public $number;
    public $street;
    public $city;
    public $state;
    public $zip;
    public $country;

    private $table = 'CORE_PLACES';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * toma los valores de cada propiedad para ajustarlas al arreglo que
     * deberá ser pasado a la base de datos
     * @return arreglo un arreglo con las llaves que son nombres de los campos y sus respectivos valores
     */
    public function import() {
        return array(
            'place_id'         => $this->id,
            'place_maps_id'    => $this->mapsId,
            'place_maps_coord' => $this->mapsCord,
            'place_maps_name'  => $this->name,
            'place_number'     => $this->number,
            'place_street'     => $this->street,
            'place_city'       => $this->city,
            'place_state'      => $this->state,
            'place_zip'        => $this->zip,
            'place_country'    => $this->country
        );
    }

    /**
     * pone las propiedades de la clase con los valores obtenidos de una consulta a la base de datos
     * @param registro objeto que se obtiene de una consulta de base de datos
     */
    public function export($registro) {
        $this->id        = $registro->place_id;
        $this->mapsId    = $registro->place_maps_id;
        $this->mapsCord  = $registro->place_maps_coord;
        $this->name      = $registro->place_maps_name;
        $this->number    = $registro->place_number;
        $this->street    = $registro->place_street;
        $this->city      = $registro->place_city;
        $this->state     = $registro->place_state;
        $this->zip       = $registro->place_zip;
        $this->country   = $registro->place_country;
    }

    /**
     * agrega un nuevo registro de llamada a la base de datos
     * @return result true si se realiza la insercion
     */
    public function add() {
        $result = false;
        try {
            $data = $this->import();
            $this->db->set($data);
            $this->db->insert( $this->table );
            $this->id = $this->db->insert_id();
            $result = true;
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
        }
        return $result;
    }

    /**
     * actualiza un nuevo registro de llamada a la base de datos
     * @return result true si se ejecuta correctamente la actualización
     */
    public function update() {
        $result = false;
        if ( !is_null( $this->id ) ) {
            try {
                $data = $this->import();
                $this->db->where('place_id',$this->id);
                $this->db->update( $this->table ,$data);
                $result = true;
            }
            catch ( Exception $ex ) {
                echo $ex->getMessage();
            }
        }
        return $result;
    }

    /**
     * guarda un registro, ya sea nuevo o actualizado
     * @return result true cuando se guarda el registro
     */
    public function save() {
        if ( is_null( $this->id ) ) {
            $result = $this->add();
        }
        else {
            $result = $this->update();
        }
        return $result;
    }

    /**
     * limpia las propiedades del objeto, dejando todas en null
     */
    public function clean() {
        $this->id        = null;
        $this->mapsId    = null;
        $this->mapsCord  = null;
        $this->name      = null;
        $this->number    = null;
        $this->street    = null;
        $this->city      = null;
        $this->state     = null;
        $this->zip       = null;
        $this->country   = null;
    }

    /**
     * encuentra un lugar por su id y settea las propiedades del objeto
     * si no es encontrado, el objeto quedara en nulos
     * @param id el ID a buscar
     */
    public function findById($id) {
        $this->clean();
        $this->db->where('place_id',$id);
        $query = $this->db->get( $this->table, 1 );
        $data = $query->result();
        foreach( $data as $row ) {
            $this->export($row);
        }
    }

    /**
     * encuentra un lugar por su nombre y settea las propiedades del objeto
     * si no es encontrado el objeto quedara en nulos
     * @param name el nombre a buscar
     */
    public function findByName($name) {
        $this->clean();
        $this->db->where('place_maps_name',$name);
        $query = $this->db->get( $this->table, 1 );
        $data = $query->result();
        foreach( $data as $row ) {
            $this->export($row);
        }
    }

    /**
     * busca o agrega un registro de lugar; si no lo encuentra intentara agregar con los datos proporcionados
     * @param data un arreglo asociativo con la informacion a agregar
     */
    public function findOrAdd($data) {
        $this->findByName($data['name']);
        if ( is_null( $this->id ) ) {
            $this->mapsId = $data['mapsId'];
            $this->mapsCord = $data['latitude'].','.$data['longitude'];
            $this->name = $data['name'];
            $this->number = $data['number'];
            $this->street = $data['street'];
            $this->city = $data['city'];
            $this->state = $data['state'];
            $this->zip = $data['zip'];
            $this->country = $data['country'];
            $this->add();
        }
    }

}