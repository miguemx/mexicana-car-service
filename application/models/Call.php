<?php

class Call extends CI_Model {

    public $id;
    public $dayNumber;
    public $line = null;
    public $dateTime;
    public $timestamp;
    public $phone = null;
    public $status;
    public $dispatchDateTime = null;
    public $dispatchTimestamp = null;
    public $dispatchType = null;
    public $registerType = null;
    public $operator = null;
    public $carType = null;

    private $tableCalls = 'CORE_CALLS';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * toma los valores de cada propiedad para ajustarlas al arreglo que
     * deberá ser pasado a la base de datos
     * @return arreglo un arreglo con las llaves que son nombres de los campos y sus respectivos valores
     */
    public function import() {
        return array(
            'call_day_number'         => $this->dayNumber,
            'call_line'               => $this->line,
            'call_date_time'          => $this->dateTime,
            'call_timestamp'          => $this->timestamp,
            'call_phone'              => $this->phone,
            'call_status'             => $this->status,
            'call_dispatch_date_time' => $this->dispatchDateTime,
            'call_dispatch_timestamp' => $this->dispatchTimestamp,
            'call_dispatch_type'      => $this->dispatchType,
            'call_register_type'      => $this->registerType,
            'call_operator'           => $this->operator,
            'call_car_type'           => $this->carType
        );
    }

    /**
     * pone las propiedades de la clase con los valores obtenidos de una consulta a la base de datos
     * @param registro objeto que se obtiene de una consulta de base de datos
     */
    public function export($registro) {
        $this->id                = $registro->call_id;
        $this->dayNumber         = $registro->call_day_number;
        $this->line              = $registro->call_line;
        $this->dateTime          = $registro->call_date_time;
        $this->timestamp         = $registro->call_timestamp;
        $this->phone             = $registro->call_phone;
        $this->status            = $registro->call_status;
        $this->dispatchDateTime  = $registro->call_dispatch_date_time;
        $this->dispatchTimestamp = $registro->call_dispatch_timestamp;
        $this->dispatchType      = $registro->call_dispatch_type;
        $this->registerType      = $registro->call_register_type;
        $this->operator          = $registro->call_operator;
        $this->carType           = $registro->call_car_type;
    }

    /**
     * agrega un nuevo registro de llamada a la base de datos
     * @return result true si se realiza la insercion
     */
    public function add() {
        $result = false;
        try {
            // obtener las fechas necesarias
            $this->timestamp = time();
            $this->dateTime = date( 'Y-m-d H:i:s', $this->timestamp );
            // obtener el consecutivo diario
            $this->dayNumber = $this->calculateDayNumber();
            // con todos los datos listos, se hace el guardado en base de datos
            $data = $this->import();
            $this->db->set($data);
            $this->db->insert( $this->tableCalls );
            $this->id = $this->db->insert_id();
            $result = true;
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
        }
        return $result;
    }

    /**
     * actualiza un nuevo registro de llamada a la base de datos
     * @return result true si se ejecuta correctamente la actualización
     */
    public function update() {
        $result = false;
        if ( !is_null( $this->id ) ) {
            try {
                $data = $this->import();
                $this->db->where('call_id',$this->id);
                $this->db->update( $this->tableCalls ,$data);
                $result = true;
            }
            catch ( Exception $ex ) {
                echo $ex->getMessage();
            }
        }
        return $result;
    }

    public function delete() {
        
    }

    /**
     * guarda un registro, ya sea nuevo o actualizado
     * @return result true cuando se guarda el registro
     */
    public function save() {
        if ( is_null( $this->id ) ) {
            $result = $this->add();
        }
        else {
            $result = $this->update();
        }
        return $result;
    }

    /**
     * limpia el objeto dejando todas sus propiedades con nulos
     */
    public function clean() {
        $this->id                = null;
        $this->dayNumber         = null;
        $this->line              = null;
        $this->dateTime          = null;
        $this->timestamp         = null;
        $this->phone             = null;
        $this->status            = null;
        $this->dispatchDateTime  = null;
        $this->dispatchTimestamp = null;
        $this->dispatchType      = null;
        $this->registerType      = null;
        $this->operator          = null;
        $this->carType           = null;
    }

    /**
     * obtiene el siguiente numero de llamada para el dia actual
     */
    private function calculateDayNumber() {
        $next = 0;
        $this->db->select_max( 'call_day_number' );
        $this->db->like('call_date_time', date('Y-m-d'), 'after');
        $query = $this->db->get( $this->tableCalls );
        
        $data = $query->result();
        foreach($data as $row) {
            $next = $row->call_day_number + 1;
        }
        return $next;
    }

    public function getData() {
        $calls = array();
        $this->db->order_by('call_id DESC, call_day_number DESC');
        $query = $this->db->get( $this->tableCalls, 250 );
        $data = $query->result();
        foreach( $data as $row ) {
            $this->export($row);
            $calls[] = array(
                'id' => $this->id,                
                'dayNumber' => $this->dayNumber,         
                'line' => $this->line,              
                'dateTime' => $this->dateTime,          
                'timestamp' => $this->timestamp,         
                'phone' => $this->phone,             
                'status' => $this->status,            
                'dispatchDateTime' => $this->dispatchDateTime,  
                'dispatchTimestamp' => $this->dispatchTimestamp, 
                'dispatchType' => $this->dispatchType,      
                'registerType' => $this->registerType,      
                'operator' => $this->operator,          
                'carType' => $this->carType,
            );
        }
        return $calls;
    }

    public function getAllData() {
        $calls = array();
        $this->db->order_by( 'call_id DESC, call_day_number DESC' );
        $query = $this->db->get( 'view_call_information', 250 );
        $data = $query->result();
        foreach ( $data as $row ) {
            $calls[] = array(
                'id' => $row->call_id,
                'operator' => $row->call_operator,
                'cartype' => $row->call_car_type,
                'clientid' => $row->call_client,
                'daynumber' => $row->call_day_number,
                'line' => $row->call_line,
                'datecall' => $row->call_date_time,
                'calltimestamp' => $row->call_timestamp,
                'phone' => $row->call_phone,
                'status' => $row->call_status,
                'dispatchdate' => $row->call_dispatch_date_time,
                'dispatchtype' => $row->call_dispatch_type,
                'registertype' => $row->call_register_type,

                'tripid' => $row->trip_id,
                'vehicle' => $row->trip_vehicle,
                'tripstatus' => $row->trip_status,
                'amount' => $row->trip_amount,
                'eta' => $row->trip_eta,
                'tripstart' => $row->trip_start_time,
                'tripend' => $row->trip_end_time,
                'distance' => $row->trip_distance,

                'cartypename' => $row->cartype_name,
                'passengernumber' => $row->cartype_pass_number,

                'username' => $row->user_username,

                'carplate' => $row->vehicle_plate,
                'caryear' => $row->vehicle_year,
                'carmodel' => $row->vehicle_model,
                'carmake' => $row->vehicle_make,
                'carcolor' => $row->vehicle_color,
                'carowned' => $row->vehicle_owned,
                'driverimei' => $row->driver_imei,
                'drivernip' => $row->driver_pin,
                'driverlicence' => $row->driver_licence,
                'driverstatus' => $row->driver_status,
                
                'pickupmapsid' => $row->pickup_maps_id,
                'pickupmapscoord' => $row->pickup_maps_coord,
                'pickupname' => $row->pickup_maps_name,
                'pickupnumber' => $row->pickup_number,
                'pickupstreet' => $row->pickup_street,
                'pickupcity' => $row->pickup_city,
                'pickupstate' => $row->pickup_state,
                'pickupzip' => $row->pickup_zip,
                'pickupcountry' => $row->pickup_country,
                
                'destmapsid' => $row->dest_maps_id,
                'destmapscoord' => $row->dest_maps_coord,
                'destname' => $row->dest_maps_name,
                'destnumber' => $row->dest_number,
                'deststreet' => $row->dest_street,
                'destcity' => $row->dest_city,
                'deststate' => $row->dest_state,
                'destzip' => $row->dest_zip,
                'destcountry' => $row->dest_country,
            );
        }
        return $calls;
    }

    public function searchAllData($id) {
        $calls = array();
        $this->db->where( 'call_id', $id );
        $this->db->order_by( 'call_id desc, call_day_number desc' );
        $query = $this->db->get( 'view_call_information', 250 );
        $data = $query->result();
        foreach ( $data as $row ) {
            // var_dump($row);
            $calls[] = array(
                'id' => $row->call_id,
                'operator' => $row->call_operator,
                'cartype' => $row->call_car_type,
                'clientid' => $row->call_client,
                'daynumber' => $row->call_day_number,
                'line' => $row->call_line,
                'datecall' => $row->call_date_time,
                'calltimestamp' => $row->call_timestamp,
                'phone' => $row->call_phone,
                'status' => $row->call_status,
                'dispatchdate' => $row->call_dispatch_date_time,
                'dispatchtype' => $row->call_dispatch_type,
                'registertype' => $row->call_register_type,

                'tripid' => $row->trip_id,
                'vehicle' => $row->trip_vehicle,
                'tripstatus' => $row->trip_status,
                'amount' => $row->trip_amount,
                'eta' => $row->trip_eta,
                'tripstart' => $row->trip_start_time,
                'tripend' => $row->trip_end_time,
                'distance' => $row->trip_distance,

                'cartypename' => $row->cartype_name,
                'passengernumber' => $row->cartype_pass_number,
                
                'username' => $row->user_username,

                'carplate' => $row->vehicle_plate,
                'caryear' => $row->vehicle_year,
                'carmodel' => $row->vehicle_model,
                'carmake' => $row->vehicle_make,
                'carcolor' => $row->vehicle_color,
                'carowned' => $row->vehicle_owned,
                // 'carstatus' => $row->vehicle_status,
                'driverperson' => $row->driver_person,
                'driverimei' => $row->driver_imei,
                // 'drivernip' => $row->driver_nip,
                'driverlicence' => $row->driver_licence,
                'driverstatus' => $row->driver_status,
                
                'pickupmapsid' => $row->pickup_maps_id,
                'pickupmapscoord' => $row->pickup_maps_coord,
                'pickupname' => $row->pickup_maps_name,
                'pickupnumber' => $row->pickup_number,
                'pickupstreet' => $row->pickup_street,
                'pickupcity' => $row->pickup_city,
                'pickupstate' => $row->pickup_state,
                'pickupzip' => $row->pickup_zip,
                'pickupcountry' => $row->pickup_country,
                
                'destmapsid' => $row->dest_maps_id,
                'destmapscoord' => $row->dest_maps_coord,
                'destname' => $row->dest_maps_name,
                'destnumber' => $row->dest_number,
                'deststreet' => $row->dest_street,
                'destcity' => $row->dest_city,
                'deststate' => $row->dest_state,
                'destzip' => $row->dest_zip,
                'destcountry' => $row->dest_country,
            );
        }
        return $calls;
    }

    public function searchNewData($id) {
        $calls = array();
        $this->db->where( 'call_id', $id );
        $this->db->order_by( 'call_id desc, call_day_number desc' );
        $query = $this->db->get( 'view_call_information', 250 );
        $data = $query->result();
        foreach ( $data as $row ) {
            $calls[] = array(
                'id' => $row->call_id,
                'daynumber' => $row->call_day_number,
                'line' => $row->call_line,
                'datecall' => $row->call_date_time,
                'calltimestamp' => $row->call_timestamp,
                'phone' => $row->call_phone,
                'status' => $row->call_status,
                'dispatchdate' => $row->call_dispatch_date_time,
                'dispatchtype' => $row->call_dispatch_type,
                'registertype' => $row->call_register_type,
                'amount' => $row->trip_amount,
                'eta' => $row->trip_eta,
                'distance' => $row->trip_distance,
                'username' => $row->user_username,
                'pickupmapsid' => $row->pickup_maps_id,
                'pickupmapscoord' => $row->pickup_maps_coord,
                'pickupname' => $row->pickup_maps_name,
                'pickupnumber' => $row->pickup_number,
                'pickupstreet' => $row->pickup_street,
                'pickupcity' => $row->pickup_city,
                'pickupstate' => $row->pickup_state,
                'pickupzip' => $row->pickup_zip,
                'pickupcountry' => $row->pickup_country,
                'destmapsid' => $row->dest_maps_id,
                'destmapscoord' => $row->dest_maps_coord,
                'destname' => $row->dest_maps_name,
                'destnumber' => $row->dest_number,
                'deststreet' => $row->dest_street,
                'destcity' => $row->dest_city,
                'deststate' => $row->dest_state,
                'destzip' => $row->dest_zip,
                'destcountry' => $row->dest_country,
            );
        }
        return $calls;
    }
}
